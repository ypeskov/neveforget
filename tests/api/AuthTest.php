<?php

use App\User;
use Tests\TestCase;
use Tests\Traits\UserFactoryTrait;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 6/10/16
 * Time: 12:09 PM
 */
class AuthTest extends TestCase
{
    use UserFactoryTrait;

    public function testRegisterWithValidationErrors()
    {
        $this
            ->postJson(route('api.auth.register'), [])
            ->assertStatus(400)
            ->assertErrorKeysExist([
                'first_name',
                'email',
                'password'
            ]);

        $this
            ->postJson(route('api.auth.register'), [
                'first_name' => str_repeat('long', 60),
                'email' => 'invalid',
                'password' => 'short'
            ])
            ->assertStatus(400)
            ->assertErrorKeysExist([
                'email',
                'password',
            ])
            ->assertErrorKeysNotExist([
                'first_name',
            ]);
    }

    public function testRegisterWithNonUniqueEmail()
    {
        // duplicate email
        $user = $this->createUser();

        $this
            ->postJson(route('api.auth.register'), [
                'first_name' => 'correct first_name',
                'email' => $user->email,
                'password' => 'correct password',
            ])
            ->assertStatus(400)
            ->assertErrorKeysExist([
                'email',
            ])
            ->assertErrorKeysNotExist([
                'password',
                'first_name',
            ]);
    }

    public function testGetUserInfoByGuest()
    {
        $this
            ->getJson(route('api.auth.user'))
            ->assertStatus(401)
            ->assertKeysNotExist([
                'result.data.id',
                'result.data.first_name',
                'result.data.email',
            ]);
    }

    public function testRegister()
    {
        $password = str_random(10);

        /**
         * @var User
         */
        $user = $this->makeUser();

        $token = $this
            ->postJson(route('api.auth.register'), [
                    'first_name' => $user->first_name,
                    'email' => $user->email,
                    'password' => $password,
                ]
            )
            ->assertStatus(200)
            ->assertKeyExists('result.data.token')
            ->assertKeyNotExists('result.data.password')
            ->get('result.data.token');

        $this
            ->getJson(route('api.auth.user'), [
                'Authorization' => "Bearer {$token}",
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'first_name' => $user->first_name,
                'email' => $user->email,
            ])
            ->assertKeyNotExists('result.data.password');
    }

    public function testLogin()
    {
        $user = $this->createUser([
            'password' => bcrypt('password'),
        ]);

        $this
            ->postJson(route('api.auth.login'), [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.token');
    }

    public function testLoginWithWrongCredentials()
    {
        $user = $this->createUser();

        $this
            ->postJson(route('api.auth.login'), [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertStatus(422)
            ->assertKeyNotExists('result.data.id');
    }

    public function testLoginLogout()
    {
        $password = str_random(10);

        /** @var User $user */
        $user = $this->createUser([
            'password' => bcrypt($password),
        ]);

        $token = $this
            ->postJson(route('api.auth.login'), [
                'email' => $user->email,
                'password' => $password,
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.token')
            ->assertKeyNotExists('result.data.password')
            ->get('result.data.token');

        $this
            ->getJson(route('api.auth.user'), [
                'Authorization' => "Bearer {$token}",
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $user->id,
                'first_name' => $user->first_name,
                'email' => $user->email,
            ])
            ->assertKeyNotExists('result.data.password');

        $this
            ->postJson(route('api.auth.logout'), [], [
                'Authorization' => "Bearer {$token}",
            ])
            ->assertStatus(200);

        // user info after logout
        $this
            ->getJson(route('api.auth.user'), [
                'Authorization' => "Bearer {$token}",
            ])
            ->assertStatus(401)
            ->assertKeysNotExist([
                'result.data.id',
                'result.data.first_name',
                'result.data.email',
                'result.data.password'
            ]);
    }
}