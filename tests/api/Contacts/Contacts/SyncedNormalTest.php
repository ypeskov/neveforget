<?php

namespace Tests\Contacts;

use App\Models\Contact;
use PHPUnit_Framework_Assert as PHPUnit;
use Tests\Traits\UserFactoryTrait;
use Tests\Traits\ContactsTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;
use Tests\Traits\SocialTrait;

class SyncedNormalTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use ContactsTrait;
    use SocialTrait;

    /**
     * @test
     */
    public function show_should_show_overridden_data()
    {
        $person = $this->createSyncedPerson();
        $originalContact = $this->createContactWithOverridden($person->id, [], ['value' => 'overridden-value']);
        $overriddenContact = $originalContact->overriddenContact;

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.contacts.show', [$person->id, $originalContact->id]))
            ->assertStatus(200)
            ->dump()
            ->assertDataKeysEqual([
                'id' => $originalContact->id,
                'value' => $overriddenContact->value,
                'contact_type' => $overriddenContact->contact_type,
                'person_id' => $person->id,
            ])->assertKeysNotExist([
                'result.data.overridden_contact',
            ]);
    }

    /**
     * @test
     */
    public function update_of_synced_contact_should_create_overridden_one_if_none_exists()
    {
        $person = $this->createSyncedPerson();
        $originalContact = $this->createContact($person->id);

        $originalValue = $originalContact->value;
        $overriddenValue = $originalContact->value . '-updated';

        // check response

        $this
            ->loginAs($person->owner->id)
            ->putJson(route('api.metaperson.contacts.update', [$person->id, $originalContact->id]), [
                'value' => $overriddenValue,
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalContact->id,
                'person_id' => $person->id,

                'value' => $overriddenValue,
                'contact_type' => $originalContact->contact_type,
            ]);

        // check actual state

        $originalContact = Contact::find($originalContact->id);
        $overriddenContact = $originalContact->overriddenContact;

        PHPUnit::assertEquals($originalValue, $originalContact->value);
        PHPUnit::assertEquals($overriddenValue, $overriddenContact->value);
    }

    /**
     * @test
     */
    public function update_of_synced_contact_should_update_overridden_one_if_already_exists()
    {
        $person = $this->createSyncedPerson();
        $originalContact = $this->createContactWithOverridden($person->id, [], ['value' => 'overridden-value']);
        $overriddenContact = $originalContact->overriddenContact;

        $originalValue = $originalContact->value;
        $updatedOverriddenValue = 'overridden-UPD-value';

        // check response

        $this
            ->loginAs($person->owner->id)
            ->putJson(route('api.metaperson.contacts.update', [$person->id, $originalContact->id]), [
                'value' => $updatedOverriddenValue,
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalContact->id,
                'person_id' => $person->id,

                'value' => $updatedOverriddenValue,
                'contact_type' => $overriddenContact->contact_type,
            ]);

        // check actual state

        $originalContact = Contact::find($originalContact->id);
        $overriddenContact = Contact::find($overriddenContact->id);

        PHPUnit::assertEquals($originalValue, $originalContact->value);
        PHPUnit::assertEquals($updatedOverriddenValue, $overriddenContact->value);
    }

    /**
     * @test
     */
    public function delete_of_synced_contact_should_remove_remove_overridden_if_it_exists()
    {
        $person = $this->createSyncedPerson();
        $originalContact = $this->createContactWithOverridden($person->id, [], ['value' => 'overridden-value']);
        $overriddenContact = $originalContact->overriddenContact;

        $this
            ->loginAs($person->owner->id)
            ->deleteJson(route('api.metaperson.contacts.delete', [$person->id, $originalContact->id]))
            ->assertStatus(200);

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.contacts.show', [$person->id, $originalContact->id]))
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalContact->id,
                'value' => $originalContact->value,
                'contact_type' => $originalContact->contact_type,
                'person_id' => $person->id,

                'overridden_contact' => null,
            ]);
    }

    /**
     * @test
     */
    public function delete_of_synced_contact_should_hide_it_if_no_overridden_exists()
    {
        $person = $this->createSyncedPerson();
        $contact = $this->createContact($person->id);

        $this
            ->loginAs($person->owner->id)
            ->deleteJson(route('api.metaperson.contacts.delete', [$person->id, $contact->id]))
            ->assertStatus(200);

        $contact = Contact::find($contact->id);
        PHPUnit::assertTrue($contact->is_hidden);
    }
}