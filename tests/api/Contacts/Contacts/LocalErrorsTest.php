<?php

namespace Tests\Contacts;

use Tests\Traits\UserFactoryTrait;
use Tests\Traits\ContactsTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/8/16
 * Time: 4:49 PM
 */
class LocalErrorsTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use ContactsTrait;

    public function testIndexByGuest()
    {
        $person = $this->createPerson();

        $this
            ->getJson(route('api.metaperson.contacts.index', ['person' => $person->id]))
            ->assertStatus(401);
    }

    public function testShowByGuest()
    {
        $contact = $this->createContact();

        $this
            ->getJson(route('api.metaperson.contacts.show', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(401);
    }

    public function testShowByNotOwner()
    {
        $user = $this->createUser();
        $contact = $this->createContact();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metaperson.contacts.show', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(403);
    }

    public function testShowDeleted()
    {
        $contact = $this->createContact();
        $contact->delete();

        $this
            ->loginAs($contact->person->owner->id)
            ->getJson(route('api.metaperson.contacts.show', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(404)
            ->assertKeyNotExists('result.data.id');
    }

    public function testCreateByGuest()
    {
        $person = $this->createPerson();

        $this
            ->postJson(route('api.metaperson.contacts.create', ['person' => $person->id]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testCreateByNotPersonOwner()
    {
        $user = $this->createUser();
        $person = $this->createPerson();

        $this
            ->loginAs($user->id)
            ->postJson(route('api.metaperson.contacts.create', ['person' => $person->id]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testCreateWithInvalidData()
    {
        $this->markTestIncomplete(); // @todo what are validation rules for Contact
    }

    public function testUpdateByGuest()
    {
        $contact = $this->createContact();

        $this
            ->putJson(route('api.metaperson.contacts.update', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testUpdateByNotOwner()
    {
        $user = $this->createUser();
        $contact = $this->createContact();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metaperson.contacts.update', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testUpdateWithInvalidData()
    {
        $this->markTestIncomplete();
    }

    public function testUpdateDeleted()
    {
        $contact = $this->createContact();
        $contact->delete();

        $this
            ->loginAs($contact->person->owner->id)
            ->putJson(route('api.metaperson.contacts.update', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(404);
    }

    public function testDeleteByGuest()
    {
        $contact = $this->createContact();

        $this
            ->deleteJson(route('api.metaperson.contacts.delete', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(401);
    }

    public function testDeleteByNotOwner()
    {
        $user = $this->createUser();
        $contact = $this->createContact();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.metaperson.contacts.delete', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(403);
    }

    public function testDeleteDeleted()
    {
        $contact = $this->createContact();
        $contact->delete();

        $this
            ->loginAs($contact->person->owner->id)
            ->deleteJson(route('api.metaperson.contacts.delete', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(404);
    }
}