<?php

namespace Tests\Contacts;

use App\Models\Contact;
use Tests\Traits\UserFactoryTrait;
use Tests\Traits\ContactsTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/8/16
 * Time: 4:49 PM
 */
class LocalNormalTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use ContactsTrait;

    public function testIndexEmpty()
    {
        $person = $this->createPerson();

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.contacts.index', ['person' => $person->id]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testIndex()
    {
        $person = $this->createPerson();
        $this->createContact($person->id);
        $this->createContact($person->id);

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.contacts.index', ['person' => $person->id]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
            ]);
    }

    public function testIndexWithQuery()
    {
        $person = $this->createPerson();
        $contacts = [];
        $contacts[Contact::TYPE_EMAIL] = $this->createContact($person->id, [
            'contact_type' => Contact::TYPE_EMAIL,
        ]);
        $contacts[Contact::TYPE_PHONE] = $this->createContact($person->id, [
            'contact_type' => Contact::TYPE_PHONE,
        ]);

        foreach ($contacts as $contactType => $contact) {
            $this
                ->loginAs($person->owner->id)
                ->getJson(route('api.metaperson.contacts.index', ['person' => $person->id, 'contact_type' => $contactType]))
                ->assertStatus(200)
                ->assertKeyChildrenCountEquals('result.data', 1)
                ->assertKeyEquals('result.data.0.id', $contact->id);
        }
    }

    public function testShow()
    {
        $contact = $this->createContact();

        $this
            ->loginAs($contact->person->owner->id)
            ->getJson(route('api.metaperson.contacts.show', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'id' => $contact->id,
                'person_id' => $contact->person->id,
            ]);
    }

    public function testCreate()
    {
        $person = $this->createPerson();
        $contact = $this->makeContact();

        $data = [
            'contact_type' => $contact->contact_type,
            'value' => $contact->value,
        ];

        $this
            ->loginAs($person->owner->id)
            ->postJson(route('api.metaperson.contacts.create', ['person' => $person->id]), $data)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($data)
            ->assertDataKeysEqual([
                'person_id' => $person->id,
            ]);
    }

    public function testUpdate()
    {
        $contact = $this->createContact();

        $originalData = [
            'contact_type' => $contact->contact_type,
            'value' => $contact->value,
        ];

        $modifiedData = array_merge($originalData, [
            'value' => 'updated-value',
        ]);

        $this
            ->loginAs($contact->person->owner->id)
            ->putJson(route('api.metaperson.contacts.update', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($modifiedData);
    }

    public function testDelete()
    {
        $contact = $this->createContact();

        $this
            ->loginAs($contact->person->owner->id)
            ->deleteJson(route('api.metaperson.contacts.delete', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(200);

        $this
            ->loginAs($contact->person->owner->id)
            ->getJson(route('api.metaperson.contacts.show', [
                'person' => $contact->person->id,
                'contact' => $contact->id,
            ]))
            ->assertStatus(404);
    }
}