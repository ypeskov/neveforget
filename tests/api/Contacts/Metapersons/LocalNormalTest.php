<?php

namespace Tests\Metapersons;

use Tests\Traits\UserFactoryTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 07.8.16
 * Time: 14:02
 *
 * @todo test contacts attribute
 */
class LocalNormalTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;

    public function testIndexEmpty()
    {
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testIndex()
    {
        $user = $this->createUser();
        $this->createPerson($user->id);
        $this->createPerson($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
            ]);
    }

    public function testIndexWithQuery()
    {
        $user = $this->createUser();

        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                    'id' => [
                        $persons[1]->id,
                        $persons[2]->id,
                    ]
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertDataKeysEqual([
                '0.id' => $persons[1]->id,
                '1.id' => $persons[2]->id,
            ]);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                    'is_local_type' => true,
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 3);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                    'is_local_type' => false,
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testShow()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'id' => $metaperson->id,
                'owner_id' => $metaperson->owner_id,
            ]);
    }

    public function testCreate()
    {
        $user = $this->createUser();
        $metaperson = $this->makePerson();

        $data = [
            'first_name' => $metaperson->first_name,
            'last_name' => $metaperson->last_name,
            'nick_name' => $metaperson->nick_name,
            'notes' => $metaperson->notes,
            'job_title' => $metaperson->job_title,
            'company' => $metaperson->company,
        ];

        $this
            ->loginAs($user->id)
            ->postJson(route('api.metapersons.create'), $data)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($data)
            ->assertDataKeysEqual([
                'owner_id' => $user->id
            ]);
    }

    public function testUpdate()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);

        $originalData = [
            'metaperson' => [
                'first_name' => $metaperson->first_name,
                'last_name' => $metaperson->last_name,
                'nick_name' => $metaperson->nick_name,
                'notes' => $metaperson->notes,
                'job_title' => $metaperson->job_title,
                'company' => $metaperson->company,
            ],
        ];

        $modifiedData = $originalData;

        $modifiedData['metaperson'] = array_map(function ($item) {
            return $item . ' - updated';
        }, $originalData['metaperson']);

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.metaperson.id')
            ->assertDataKeysEqual(array_dot($modifiedData))
            ->assertDataKeysEqual([
                'metaperson.id' => $metaperson->id,
            ]);
    }

    public function testUpdatePartial()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);

        $originalData = [
            'first_name' => $metaperson->first_name,
            'last_name' => $metaperson->last_name,
            'nick_name' => $metaperson->nick_name,
            'notes' => $metaperson->notes,
            'job_title' => $metaperson->job_title,
            'company' => $metaperson->company,
        ];

        $modifiedData = [
            'nick_name' => $metaperson->nick_name . '-updated',
        ];

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), [
                'metaperson' => $modifiedData,
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.metaperson.id')
            ->assertNestedKeysEqual('result.data.metaperson', array_merge($originalData, $modifiedData))
            ->assertDataKeysEqual([
                'metaperson.id' => $metaperson->id,
            ]);
    }

    public function testDelete()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);

        // should be shown in index
        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 1);

        // delete
        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.metapersons.delete', ['id' => $metaperson->id]))
            ->assertStatus(200);

        // should not be shown in index
        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);

        // still is deleted (@todo think about soft deleting)
        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(404);
    }

    public function testRestore()
    {
        $this->markTestIncomplete();
    }
}