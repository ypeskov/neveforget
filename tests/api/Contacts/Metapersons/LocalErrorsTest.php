<?php

namespace Tests\Metapersons;

use Tests\Traits\PersonsTrait;
use Tests\Traits\UserFactoryTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 07.8.16
 * Time: 14:02
 *
 * @todo test contacts attribute
 */
class LocalErrorsTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;

    public function testIndexByGuest()
    {
        $this
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(401);
    }

    public function testShowByGuest()
    {
        $metaperson = $this->createPerson();

        $this
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(401);
    }

    public function testShowByNotOwner()
    {
        $metaperson = $this->createPerson();
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(403);
    }

    public function testShowDeleted()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);
        $metaperson->delete();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(404)
            ->assertKeyNotExists('result.data.id');
    }

    public function testCreateByGuest()
    {
        $this
            ->postJson(route('api.metapersons.create'), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testCreateWithInvalidData()
    {
        $user = $this->createUser();

        $data = [
            // 'first_name' - required
            'last_name' => str_repeat('x', 256),
            'nick_name' => str_repeat('x', 256),
            'notes' => 123, // not string
            'job_title' => str_repeat('x', 256),
            'company' => str_repeat('x', 256),
        ];

        $this
            ->loginAs($user->id)
            ->postJson(route('api.metapersons.create'), $data)
            ->assertStatus(400)
            ->assertKeysExist([
                'result.errors.first_name',
                'result.errors.last_name',
                'result.errors.nick_name',
                'result.errors.notes',
                'result.errors.job_title',
                'result.errors.company',
            ])
            ->assertKeyNotExists('result.data.id');
    }

    public function testUpdateByGuest()
    {
        $metaperson = $this->createPerson();

        $this
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testUpdateByNotOwner()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testUpdateWithInvalidData()
    {
        $metaperson = $this->createPerson();

        $data = [
            'first_name' => str_repeat('x', 256),
            'last_name' => str_repeat('x', 256),
            'nick_name' => str_repeat('x', 256),
            'notes' => 123, // not string
            'job_title' => str_repeat('x', 256),
            'company' => str_repeat('x', 256),
        ];

        $this
            ->loginAs($metaperson->owner->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), $data)
            ->assertStatus(400)
            ->assertKeysExist([
                'result.errors.first_name',
                'result.errors.last_name',
                'result.errors.nick_name',
                'result.errors.notes',
                'result.errors.job_title',
                'result.errors.company',
            ])
            ->assertKeyNotExists('result.data.id');
    }

    public function testUpdateDeleted()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);
        $metaperson->delete();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), [
                'some' => 'data'
            ])
            ->assertStatus(404);
    }

    public function testDeleteByGuest()
    {
        $metaperson = $this->createPerson();

        $this
            ->deleteJson(route('api.metapersons.delete', ['id' => $metaperson->id]))
            ->assertStatus(401);
    }

    public function testDeleteByNotOwner()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.metapersons.delete', ['id' => $metaperson->id]))
            ->assertStatus(403);
    }

    public function testDeleteDeleted()
    {
        $user = $this->createUser();
        $metaperson = $this->createPerson($user->id);
        $metaperson->delete();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.metapersons.delete', ['id' => $metaperson->id]))
            ->assertStatus(404);
    }
}