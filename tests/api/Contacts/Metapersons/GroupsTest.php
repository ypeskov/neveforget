<?php

namespace Tests\Metapersons;

use Tests\Traits\GroupFactoryTrait;
use Tests\Traits\UserFactoryTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

class GroupsTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use GroupFactoryTrait;

    public function testPersonHasEmptyGroups()
    {
        $person = $this->createPerson();

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metapersons.show', [$person->id]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.groups')
            ->assertKeyChildrenCountEquals('result.data.groups', 0);
    }

    public function testPersonHasGroups()
    {
        $person = $this->createPerson();
        $group1 = $this->createGroup($person->owner->id);
        $group2 = $this->createGroup($person->owner->id);

        $person->groups()->attach($group1->id);
        $person->groups()->attach($group2->id);

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metapersons.show', [$person->id]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.groups')
            ->assertKeyChildrenCountEquals('result.data.groups', 2);
    }

    public function testIndexWithoutFilterShowsAll()
    {
        $user = $this->createUser();
        $group1 = $this->createGroup($user->id);
        $group2 = $this->createGroup($user->id);

        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);

        $persons[0]->groups()->attach($group1);
        $persons[1]->groups()->attach($group2);
        $persons[2]->groups()->attach($group2);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 4)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
                'result.data.2.id',
                'result.data.3.id',
            ]);
    }

    public function testIndexWithQuery()
    {
        $user = $this->createUser();
        $group1 = $this->createGroup($user->id);
        $group2 = $this->createGroup($user->id);

        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);
        $persons[] = $this->createPerson($user->id);

        $persons[0]->groups()->attach($group1);
        $persons[1]->groups()->attach($group2);
        $persons[2]->groups()->attach($group2);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                    'group' => $group1->id,
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 1)
            ->assertDataKeysEqual([
                '0.id' => $persons[0]->id,
            ]);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                    'group' => $group2->id,
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertDataKeysEqual([
                '0.id' => $persons[1]->id,
                '1.id' => $persons[2]->id,
            ]);
    }

    public function testCreate()
    {
        $person = $this->createPerson();

        $titles = [
            ['title' => 'A Super group one'],
            ['title' => 'B Not so good second group'],
        ];

        $data = [
            'first_name' => $person->first_name,
            'last_name' => $person->last_name,
            'nick_name' => $person->nick_name,
            'notes' => $person->notes,
            'job_title' => $person->job_title,
            'company' => $person->company,
        ];

        $groups = $this
            ->loginAs($person->owner->id)
            ->postJson(route('api.metapersons.create'), array_merge($data, [
                'groups' => $titles,
            ]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($data)
            ->assertKeyChildrenCountEquals('result.data.groups', 2)
            ->assertDataKeysEqual([
                'owner_id' => $person->owner->id,
                'groups.0.owner_id' => $person->owner->id,
                'groups.1.owner_id' => $person->owner->id,
            ])
            ->get('result.data.groups');

        $expectedTitles = array_column($titles, 'title');
        $actualTitles = array_column($groups, 'title');
        sort($expectedTitles);
        sort($actualTitles);

        \PHPUnit_Framework_Assert::assertArraySubset($expectedTitles, $actualTitles);
    }

    public function testUpdate()
    {
        $person = $this->createPerson();

        $titles = [
            ['title' => 'A Super group one'],
            ['title' => 'B Not so good second group'],
        ];

        $groups = $this
            ->loginAs($person->owner->id)
            ->putJson(route('api.metapersons.update', [$person->id]), [
                'metaperson' => [
                    'groups' => $titles,
                ],
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.metaperson.id')
            ->assertKeyChildrenCountEquals('result.data.metaperson.groups', 2)
            ->assertDataKeysEqual([
                'metaperson.owner_id' => $person->owner->id,
                'metaperson.groups.0.owner_id' => $person->owner->id,
                'metaperson.groups.1.owner_id' => $person->owner->id,
            ])
            ->get('result.data.metaperson.groups');

        $expectedTitles = array_column($titles, 'title');
        $actualTitles = array_column($groups, 'title');
        sort($expectedTitles);
        sort($actualTitles);

        \PHPUnit_Framework_Assert::assertArraySubset($expectedTitles, $actualTitles);
    }

}