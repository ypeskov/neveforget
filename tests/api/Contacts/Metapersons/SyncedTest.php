<?php

namespace Tests\Metapersons;

use Tests\Traits\PersonsTrait;
use Tests\Traits\SocialTrait;
use Tests\TestCase;
use Tests\Traits\UserFactoryTrait;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 11.9.16
 * Time: 17:06
 */
class SyncedTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use SocialTrait;

    public function testFactory()
    {
        // default state is not hidden
        $metaperson = $this->createSyncedPerson();

        $this->assertNotTrue($metaperson->is_hidden);

        // hidden state
        $metaperson = $this->createSyncedPerson(null, null, [
            'is_hidden' => true,
        ]);

        $this->assertTrue($metaperson->is_hidden);
    }

    public function testNotHiddenIsShownInIndex()
    {
        $metaperson = $this->createSyncedPerson();

        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 1);
    }

    public function testHiddenIsNotShownInIndex()
    {
        $metaperson = $this->createSyncedPerson(null, null, [
            'is_hidden' => true,
        ]);

        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testIndexWithQuery()
    {
        $user = $this->createUser();
        $social = $this->createSocial($user->id);

        $persons[] = $this->createSyncedPerson($user->id, $social->id);
        $persons[] = $this->createSyncedPerson($user->id, $social->id);
        $persons[] = $this->createSyncedPerson($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                'social_source_id' => $social->id,
            ]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertDataKeysEqual([
                '0.id' => $persons[0]->id,
                '1.id' => $persons[1]->id,
            ]);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                'is_local_type' => true,
            ]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index', [
                'is_local_type' => false,
            ]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 3);
    }

    public function testHiddenIsShownInIndexIfRequested()
    {
        $this->markTestIncomplete('Think how to show hidden persons');
    }

    public function testNotHiddenIsShownInShow()
    {
        $metaperson = $this->createSyncedPerson();

        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'is_hidden' => false,
            ]);
    }

    public function testHiddenIsShownInShow()
    {
        $metaperson = $this->createSyncedPerson(null, null, [
            'is_hidden' => true,
        ]);

        $this
            ->loginAs($metaperson->owner->id)
            ->getJson(route('api.metapersons.show', ['id' => $metaperson->id]))
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'is_hidden' => true,
            ]);
    }

    public function testDelete()
    {
        $metaperson = $this->createSyncedPerson();

        $this
            ->loginAs($metaperson->owner->id)
            ->deleteJson(route('api.metapersons.delete', ['id' => $metaperson->id]))
            ->assertStatus(200);

        $metaperson = \App\Models\Person::find($metaperson->id);
        $this->assertTrue($metaperson->is_hidden);
    }

    public function testRestore()
    {
        $this->markTestIncomplete('First name is required - fix this for update action');

        $metaperson = $this->createSyncedPerson(null, null, [
            'is_hidden' => true,
        ]);

        // restore
        $this
            ->loginAs($metaperson->owner->id)
            ->putJson(route('api.metapersons.update', ['id' => $metaperson->id]), [
                'is_hidden' => false
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'is_hidden' => false,
            ]);

        $metaperson = \App\Models\Person::find($metaperson->id);
        $this->assertFalse($metaperson->is_hidden);
    }

    public function testMergeWithoutLocal()
    {
        $user = $this->createUser();
        $persons[] = $this->createSyncedPerson($user->id);
        $persons[] = $this->createSyncedPerson($user->id);
        $persons[] = $this->createSyncedPerson($user->id);
        $ids = array_pluck($persons, 'id');

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 3)
            ->assertDataKeysEqual([
                '0.parent_person_id' => null,
                '1.parent_person_id' => null,
                '2.parent_person_id' => null,
            ]);

        $metapersonId = $this
            ->loginAs($user->id)
            ->postJson(route('api.metapersons.merge', [
                    'metapersons' => $ids,
                ]
            ))
            ->assertKeyEquals('result.type', 'person')
            ->assertDataKeysEqual([
                'social_source_id' => null,
            ])
            ->getId();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metapersons.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 1);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metaperson.persons.index', ['metaperson' => $metapersonId]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 4)
            ->assertDataKeysEqual([
                '0.parent_person_id' => $metapersonId,
                '1.parent_person_id' => $metapersonId,
                '2.parent_person_id' => $metapersonId,
                '3.parent_person_id' => null,
            ]);
    }
}