<?php

namespace Tests\Dates;

use Tests\Traits\UserFactoryTrait;
use Tests\Traits\DatesTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/8/16
 * Time: 4:49 PM
 */
class LocalErrorsTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use DatesTrait;

    public function testIndexByGuest()
    {
        $person = $this->createPerson();

        $this
            ->getJson(route('api.metaperson.dates.index', ['person' => $person->id]))
            ->assertStatus(401);
    }

    public function testShowByGuest()
    {
        $date = $this->createDate();

        $this
            ->getJson(route('api.metaperson.dates.show', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(401);
    }

    public function testShowByNotOwner()
    {
        $user = $this->createUser();
        $date = $this->createDate();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.metaperson.dates.show', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(403);
    }

    public function testShowDeleted()
    {
        $date = $this->createDate();
        $date->delete();

        $this
            ->loginAs($date->person->owner->id)
            ->getJson(route('api.metaperson.dates.show', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(404)
            ->assertKeyNotExists('result.data.id');
    }

    public function testCreateByGuest()
    {
        $person = $this->createPerson();

        $this
            ->postJson(route('api.metaperson.dates.create', ['person' => $person->id]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testCreateByNotPersonOwner()
    {
        $user = $this->createUser();
        $person = $this->createPerson();

        $this
            ->loginAs($user->id)
            ->postJson(route('api.metaperson.dates.create', ['person' => $person->id]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testCreateWithInvalidData()
    {
        $this->markTestIncomplete(); // @todo what are validation rules for Date
    }

    public function testUpdateByGuest()
    {
        $date = $this->createDate();

        $this
            ->putJson(route('api.metaperson.dates.update', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testUpdateByNotOwner()
    {
        $user = $this->createUser();
        $date = $this->createDate();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.metaperson.dates.update', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testUpdateWithInvalidData()
    {
        $this->markTestIncomplete();
    }

    public function testUpdateDeleted()
    {
        $date = $this->createDate();
        $date->delete();

        $this
            ->loginAs($date->person->owner->id)
            ->putJson(route('api.metaperson.dates.update', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]), [
                'some' => 'data',
            ])
            ->assertStatus(404);
    }

    public function testDeleteByGuest()
    {
        $date = $this->createDate();

        $this
            ->deleteJson(route('api.metaperson.dates.delete', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(401);
    }

    public function testDeleteByNotOwner()
    {
        $user = $this->createUser();
        $date = $this->createDate();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.metaperson.dates.delete', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(403);
    }

    public function testDeleteDeleted()
    {
        $date = $this->createDate();
        $date->delete();

        $this
            ->loginAs($date->person->owner->id)
            ->deleteJson(route('api.metaperson.dates.delete', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(404);
    }
}