<?php

namespace Tests\Dates;

use App\Models\Date;
use PHPUnit_Framework_Assert as PHPUnit;
use Tests\Traits\UserFactoryTrait;
use Tests\Traits\DatesTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;
use Tests\Traits\SocialTrait;

class SyncedNormalTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use DatesTrait;
    use SocialTrait;

    /**
     * @test
     */
    public function show_should_show_overridden_data()
    {
        $person = $this->createSyncedPerson();
        $originalDate = $this->createDateWithOverridden($person->id, [], ['date' => '2010-10-10 10:10:10']);
        $overriddenDate = $originalDate->overriddenDate;

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.dates.show', [$person->id, $originalDate->id]))
            ->assertStatus(200)
            ->dump()
            ->assertDataKeysEqual([
                'id' => $originalDate->id,
                'date' => $overriddenDate->date->format('Y-m-d H:i:s'),
                'date_type_id' => $overriddenDate->date_type_id,
                'person_id' => $person->id,
            ])->assertKeysNotExist([
                'result.data.overridden_date',
            ]);
    }

    /**
     * @test
     */
    public function update_of_synced_date_should_create_overridden_one_if_none_exists()
    {
        $person = $this->createSyncedPerson();
        $originalDate = $this->createDate($person->id);

        $originalValue = $originalDate->date;
        $overriddenValue = '2010-10-10 10:10:10';

        // check response

        $this
            ->loginAs($person->owner->id)
            ->putJson(route('api.metaperson.dates.update', [$person->id, $originalDate->id]), [
                'date' => $overriddenValue,
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalDate->id,
                'person_id' => $person->id,

                'date' => $overriddenValue,
                'date_type_id' => $originalDate->date_type_id,
            ]);

        // check actual state

        $originalDate = Date::find($originalDate->id);
        $overriddenDate = $originalDate->overriddenDate;

        PHPUnit::assertEquals($originalValue, $originalDate->date);
        PHPUnit::assertEquals($overriddenValue, $overriddenDate->date->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function update_of_synced_date_should_update_overridden_one_if_already_exists()
    {
        $person = $this->createSyncedPerson();
        $originalDate = $this->createDateWithOverridden($person->id, [], ['value' => '2010-10-10 10:10:10']);
        $overriddenDate = $originalDate->overriddenDate;

        $originalValue = $originalDate->date;
        $updatedOverriddenValue = '2012-12-12 12:12:12';

        // check response

        $this
            ->loginAs($person->owner->id)
            ->putJson(route('api.metaperson.dates.update', [$person->id, $originalDate->id]), [
                'date' => $updatedOverriddenValue,
            ])
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalDate->id,
                'person_id' => $person->id,

                'date' => $updatedOverriddenValue,
                'date_type_id' => $overriddenDate->date_type_id,
            ]);

        // check actual state

        $originalDate = Date::find($originalDate->id);
        $overriddenDate = Date::find($overriddenDate->id);

        PHPUnit::assertEquals($originalValue, $originalDate->date);
        PHPUnit::assertEquals($updatedOverriddenValue, $overriddenDate->date->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function delete_of_synced_date_should_remove_remove_overridden_if_it_exists()
    {
        $person = $this->createSyncedPerson();
        $originalDate = $this->createDateWithOverridden($person->id, [], ['value' => '2010-10-10 10:10:10']);
        $overriddenDate = $originalDate->overriddenDate;

        $this
            ->loginAs($person->owner->id)
            ->deleteJson(route('api.metaperson.dates.delete', [$person->id, $originalDate->id]))
            ->assertStatus(200);

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.dates.show', [$person->id, $originalDate->id]))
            ->assertStatus(200)
            ->assertDataKeysEqual([
                'id' => $originalDate->id,
                'value' => $originalDate->value,
                'date_type_id' => $originalDate->date_type_id,
                'person_id' => $person->id,

                'overridden_date' => null,
            ]);
    }

    /**
     * @test
     */
    public function delete_of_synced_date_should_hide_it_if_no_overridden_exists()
    {
        $person = $this->createSyncedPerson();
        $date = $this->createDate($person->id);

        $this
            ->loginAs($person->owner->id)
            ->deleteJson(route('api.metaperson.dates.delete', [$person->id, $date->id]))
            ->assertStatus(200);

        $date = Date::find($date->id);
        PHPUnit::assertTrue($date->is_hidden);
    }
}