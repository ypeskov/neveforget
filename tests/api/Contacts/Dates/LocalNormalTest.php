<?php

namespace Tests\Dates;

use App\Models\Date;
use Tests\Traits\UserFactoryTrait;
use Tests\Traits\DatesTrait;
use Tests\Traits\DateTypesTrait;
use Tests\Traits\PersonsTrait;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/8/16
 * Time: 4:49 PM
 */
class LocalNormalTest extends TestCase
{
    use UserFactoryTrait;
    use PersonsTrait;
    use DatesTrait;
    use DateTypesTrait;

    public function testIndexEmpty()
    {
        $person = $this->createPerson();

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.dates.index', ['person' => $person->id]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testIndex()
    {
        $person = $this->createPerson();
        $this->createDate($person->id);
        $this->createDate($person->id);

        $this
            ->loginAs($person->owner->id)
            ->getJson(route('api.metaperson.dates.index', ['person' => $person->id]))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
            ]);
    }

    public function testIndexWithQuery()
    {
        $person = $this->createPerson();
        $dateTypes = [
            $this->createDateType(),
            $this->createDateType(),
        ];

        $dates = [];
        $dates[$dateTypes[0]->id] = $this->createDate($person->id, [
            'date_type_id' => $dateTypes[0]->id,
        ]);
        $dates[$dateTypes[1]->id] = $this->createDate($person->id, [
            'date_type_id' => $dateTypes[1]->id,
        ]);

        foreach ($dates as $dateType => $date) {
            $this
                ->loginAs($person->owner->id)
                ->getJson(route('api.metaperson.dates.index', ['person' => $person->id, 'date_type_id' => $dateType]))
                ->assertStatus(200)
                ->assertKeyChildrenCountEquals('result.data', 1)
                ->assertKeyEquals('result.data.0.id', $date->id);
        }
    }

    public function testShow()
    {
        $date = $this->createDate();

        $this
            ->loginAs($date->person->owner->id)
            ->getJson(route('api.metaperson.dates.show', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'id' => $date->id,
                'person_id' => $date->person->id,
            ]);
    }

    public function testCreate()
    {
        $person = $this->createPerson();
        $date = $this->makeDate();

        $data = [
            'date_type_id' => $date->date_type_id,
            'date' => $date->date->format('Y-m-d H:i:s'),
        ];

        $this
            ->loginAs($person->owner->id)
            ->postJson(route('api.metaperson.dates.create', ['person' => $person->id]), $data)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($data)
            ->assertDataKeysEqual([
                'person_id' => $person->id,
            ]);
    }

    public function testUpdate()
    {
        $date = $this->createDate();

        $originalData = [
            'date_type_id' => $date->date_type_id,
            'date' => $date->date->format('Y-m-d H:i:s'),
        ];

        $modifiedData = array_merge($originalData, [
            'date' => $date->date->copy()->addDay(5)->format('Y-m-d H:i:s'),
        ]);

        $this
            ->loginAs($date->person->owner->id)
            ->putJson(route('api.metaperson.dates.update', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($modifiedData);
    }

    public function testDelete()
    {
        $date = $this->createDate();

        $this
            ->loginAs($date->person->owner->id)
            ->deleteJson(route('api.metaperson.dates.delete', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(200);

        $this
            ->loginAs($date->person->owner->id)
            ->getJson(route('api.metaperson.dates.show', [
                'person' => $date->person->id,
                'date' => $date->id,
            ]))
            ->assertStatus(404);
    }
}