<?php

use App\Models\Event;
use App\User;
use \Carbon\Carbon;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 6/10/16
 * Time: 12:52 PM
 */
class EventsTest extends TestCase
{
    use \Tests\Traits\UserFactoryTrait;

    public function testIndexByGuest()
    {
        $this
            ->getJson(route('api.events.index'))
            ->assertStatus(401);
    }
    
    public function testIndexEmpty()
    {
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.index', [
                    'activeDate' => '{"year":2016,"month":10,"day":18}',
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0)
            ->assertKeyNotExists('result.data.0.id');
    }

    public function testIndex()
    {
        $user = $this->createUser();

        $dt = Carbon::create();
        $dtStr = '{"year":'.$dt->year.',"month":'.$dt->month.',"day":'.$dt->day.'}';

        $this->createEvent($user->id, ['datetime_to_send' => $dt,]);
        $this->createEvent($user->id, ['datetime_to_send' => $dt,]);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.index', [
                    'activeDate'    => $dtStr,
                    'period'        => 'year',
                ]
            ))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id'
            ]);
    }

    public function testIndexWithQuery()
    {
        $user = $this->createUser();

        $dt2015     = Carbon::create(2015);
        $dt2020     = Carbon::create(2020);
        $dtStrings  = [
            '2015'  => '{"year":'.$dt2015->year.',"month":'.$dt2015->month.',"day":'.$dt2015->day.'}',
            '2020'  => '{"year":'.$dt2020->year.',"month":'.$dt2020->month.',"day":'.$dt2020->day.'}'
        ];

        $events = [];
        $events['2015'] = $this->createEvent($user->id, [
            'datetime_to_send' => $dt2015,
        ]);
        $events['2020'] = $this->createEvent($user->id, [
            'datetime_to_send' => $dt2020,
        ]);

        foreach ($events as $year => $event) {
            $this
                ->loginAs($user->id)
                ->getJson(route('api.events.index', [
                        'period'        => 'year',
                        'activeDate'    => $dtStrings[$year],
                    ]
                ))
                ->assertStatus(200)
                ->assertKeyChildrenCountEquals('result.data', 1)
                ->assertKeyEquals('result.data.0.id', $events[$year]->id);
        }
    }

    public function testShowByGuest()
    {
        $event = $this->createEvent();

        $this
            ->getJson(route('api.events.show', ['id' => $event->id]))
            ->assertStatus(401);
    }

    public function testShowByNotOwner()
    {
        $event = $this->createEvent();
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.show', ['id' => $event->id]))
            ->assertStatus(403);
    }

    public function testShow()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.show', ['id' => $event->id]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'id' => $event->id,
                'user_id' => $user->id,
            ]);
    }

    public function testShowDeleted()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);
        $event->delete();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.show', ['id' => $event->id]))
            ->assertStatus(404)
            ->assertKeyNotExists('result.data.id');
    }

    public function testCreateByGuest()
    {
        $this
            ->postJson(route('api.events.create'), [
                'some' => 'data'
            ])
            ->assertStatus(401);
    }

    public function testCreate()
    {
        $user = $this->createUser();
        $event = $this->makeEvent();

        $data = [
            'event_name' => $event->event_name,
            'recipient' => $event->recipient,
            'message_subject' => $event->message_subject,
            'message_body' => $event->message_body,
            'message_signature' => $event->message_signature,
            'datetime_to_send' => $event->datetime_to_send->format('Y-m-d H:i:s'),
            'timezone'      => 'UTC',
            'comment' => $event->comment,
        ];

        $this
            ->loginAs($user->id)
            ->postJson(route('api.events.create'), $data)
            ->assertStatus(200)
            ->assertKeyExists('result.data.event');
    }

    public function testCreateWithInvalidData()
    {
        $this->markTestIncomplete();
    }

    public function testUpdateByGuest()
    {
        $event = $this->createEvent();
        
        $this
            ->putJson(route('api.events.update', ['id' => $event->id]), [
                'some' => 'data'
            ])
            ->assertStatus(401);
    }

    public function testUpdateByNotOwner()
    {
        $user = $this->createUser();
        $event = $this->createEvent();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.events.update', ['id' => $event->id]), [
                'some' => 'data'
            ])
            ->assertStatus(403);
    }

    public function testUpdate()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);

        $originalData = [
            'event_name' => $event->event_name,
            'recipient' => $event->recipient,
            'message_subject' => $event->message_subject,
            'message_body' => $event->message_body,
            'message_signature' => $event->message_signature,
            'datetime_to_send' => $event->datetime_to_send->format('Y-m-d H:i:s'),
            'comment' => $event->comment,
        ];

        $modifiedData = array_merge($originalData, [
            'event_name'    => 'updated event name',
            'timezone'      => 'UTC',
        ]);

        $this
            ->loginAs($user->id)
            ->putJson(route('api.events.update', ['id' => $event->id]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($modifiedData);
    }

    public function testUpdateWithInvalidData()
    {
        $this->markTestIncomplete();
    }

    public function testUpdateDeleted()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);
        $event->delete();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.events.update', ['id' => $event->id]), [
                'some' => 'data'
            ])
            ->assertStatus(404);
    }

    public function testDeleteByGuest()
    {
        $event = $this->createEvent();

        $this
            ->deleteJson(route('api.events.delete', ['id' => $event->id]))
            ->assertStatus(401);
    }

    public function testDeleteByNotOwner()
    {
        $user = $this->createUser();
        $event = $this->createEvent();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.events.delete', ['id' => $event->id]), [
                'some' => 'data'
            ])
            ->assertStatus(403);
    }

    public function testDelete()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.events.delete', ['id' => $event->id]))
            ->assertStatus(200);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.events.show', ['id' => $event->id]))
            ->assertStatus(404);
    }

    public function testDeleteDeleted()
    {
        $user = $this->createUser();
        $event = $this->createEvent($user->id);
        $event->delete();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.events.delete', ['id' => $event->id]))
            ->assertStatus(404);
    }

    /**
     * Make & Save
     *
     * @param null|int $userId
     * @param array $attributes
     * @return Event
     */
    protected function createEvent($userId = null, $attributes = [])
    {
        $attributes = array_merge($attributes, [
            'user_id' => $userId ?: $this->createUser()->id,
        ]);

        return factory(Event::class)
            ->create($attributes);
    }

    /**
     * @param null|int $userId
     * @param array $attributes
     * @return Event
     */
    protected function makeEvent($userId = null, $attributes = [])
    {
        if ($userId) {
            $attributes = array_merge($attributes, [
                'user_id' => $userId,
            ]);
        }

        return factory(Event::class)
            ->make($attributes);
    }
}