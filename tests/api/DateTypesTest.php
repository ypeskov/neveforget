<?php

use Tests\TestCase;

class DateTypesTest extends TestCase
{
    use \Tests\Traits\UserFactoryTrait;

    public function testIndexByGuest()
    {
        $this
            ->getJson(route('api.data.date-types'))
            ->assertStatus(401);
    }

    public function testIndex()
    {
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.data.date-types'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 4)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
            ]);
    }

}