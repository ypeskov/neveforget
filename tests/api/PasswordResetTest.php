<?php

use App\User;
use App\Models\UserResetPassword;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 6/28/16
 * Time: 11:18 PM
 */
class PasswordResetTest extends TestCase
{
    use \Tests\Traits\MailTracking;
    use \Tests\Traits\UserFactoryTrait;

    const PASSWORD = 'password';

    public function testResetWithValidationErrors()
    {
        $this
            ->postJson(route('api.password.reset-link'), [])
            ->assertStatus(400)
            ->assertKeyExists('result.errors.resetEmail');

        $this
            ->postJson(route('api.password.reset-link'), [
                'email' => 'invalid',
            ])
            ->assertStatus(400)
            ->assertKeyExists('result.errors.resetEmail');

        // @todo is this secure?
        $this
            ->postJson(route('api.password.reset-link'), [
                'resetEmail' => 'not.registered@email.com',
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.success');
    }

   public function testResetTokenCreated()
   {
       $user = $this->createUser();
       $user->email = time() . '@example.com';
       $user->save();

       $this
           ->postJson(route('api.password.reset-link'), [
               'resetEmail'  => $user->email,
           ])
           ->assertStatus(200)
           ->assertKeyExists('result.data.success');

       $restPassword = UserResetPassword::where('user_id', $user->id)->firstOrFail();
       $this->assertEquals($user->id, $restPassword->user_id);
   }

    public function testResetShouldThrowException()
    {
        $user = $this->createUser();

        $this
            ->postJson(route('api.password.reset-link'), [
                'resetEmail'  => $user->email,
            ])
            ->assertStatus(200)
            ->assertKeyExists('result.data.success');

        $this->expectException(ModelNotFoundException::class);
        UserResetPassword::where('user_id', '---')->firstOrFail();
    }

    public function testResetFlow()
    {
        // $this->markTestIncomplete('Something went wrong with it');

        // $user = User::whereEmail('xedelweiss@gmail.com')->first();

        $user = $this->createUser([
            'password' => bcrypt(self::PASSWORD),
        ]);

        // send email

        $this->postJson(route('api.password.reset-link'), [
                'resetEmail' => $user->email,
            ])
            ->assertStatus(200)
            ->assertKeysEqual([
                'result.data.success' => true,
            ]);

        $this->seeEmailTo($user->email);

        // reset password
        $this
            ->putJson(route('api.password.set-new-password', []), [
                'token' => $this->getTokenFromEmail($this->lastEmail()),
                'email' => $user->email,
                'password' => self::PASSWORD . '-updated',
                'password_confirmation' => self::PASSWORD . '-updated',
            ])
            ->assertStatus(200)
            ->assertKeysExist([
                'result.data.success'
            ]);

        // try to login with new password

        $this
            ->postJson(route('api.auth.login'), [
                'email' => $user->email,
                'password' => self::PASSWORD . '-updated',
            ])
            ->assertStatus(200)
            ->assertKeysExist([
                'result.data.token'
            ]);
    }

    /**
     * @param \Swift_Message $email
     * @return string
     */
    protected function getTokenFromEmail($email): string
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($email->getBody());

        $finder = new \DOMXPath($dom);

        $link = $finder->query('/html/body/div/div[3]/a/@href')->item(0)->nodeValue;
        $token = substr($link, strrpos($link, '?token=') + strlen('?token='));

        return $token;
    }
}