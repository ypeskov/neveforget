<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 20.10.16
 * Time: 15:03
 */

use App\User;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use \Tests\Traits\UserFactoryTrait;

    public function testUserUpdate()
    {
        $user = $this->createUser();
        $user->timeZone = 'Europe/Kiev';

        $newUser = $user->toArray();
        $newUser['first_name']  = 'New First Name';
        $newUser['last_name']   = 'New Last Name';

        $this
            ->loginAs($user->id)
            ->putJson(route('api.users.update', [ 'id' => $user->id, ]), [
                'user'  => $newUser,
            ])
            ->assertStatus(200);

        $newUserFromDB = User::find($user->id);

        $this->assertEquals($newUser['first_name'], $newUserFromDB['first_name']);
        $this->assertEquals($newUser['last_name'], $newUserFromDB['last_name']);
    }

    public function testUserUpdateNoPermissions()
    {
        $user               = $this->createUser();
        $userNoPermissions  = $this->createUser();

        $this
            ->loginAs($userNoPermissions->id)
            ->putJson(route('api.users.update', [ 'id' => $user->id, ]), [])
            ->assertStatus(403);
    }
}