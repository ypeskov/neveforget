<?php

use Tests\TestCase;
use Tests\Traits\GroupFactoryTrait;
use Tests\Traits\UserFactoryTrait;

class GroupsTest extends TestCase
{
    use UserFactoryTrait;
    use GroupFactoryTrait;

    public function testIndexByGuest()
    {
        $group = $this->createGroup();

        $this
            ->getJson(route('api.groups.index', [$group->id]))
            ->assertStatus(401);
    }

    public function testIndexEmpty()
    {
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.groups.index'))
            ->dump()
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 0);
    }

    public function testIndex()
    {
        $user = $this->createUser();
        $this->createGroup($user->id);
        $this->createGroup($user->id);

        $this
            ->loginAs($user->id)
            ->getJson(route('api.groups.index'))
            ->assertStatus(200)
            ->assertKeyChildrenCountEquals('result.data', 2)
            ->assertKeysExist([
                'result.data.0.id',
                'result.data.1.id',
            ]);
    }

    public function testIndexWithQuery()
    {
        $this->markTestIncomplete(); // @todo implement groups index with query test
    }

    public function testShowByGuest()
    {
        $group = $this->createGroup();

        $this
            ->getJson(route('api.groups.show', [$group->id]))
            ->assertStatus(401);
    }

    public function testShowByNotOwner()
    {
        $group = $this->createGroup();
        $user = $this->createUser();

        $this
            ->loginAs($user->id)
            ->getJson(route('api.groups.show', [$group->id]))
            ->assertStatus(403);
    }

    public function testShow()
    {
        $group = $this->createGroup();

        $this
            ->loginAs($group->owner->id)
            ->getJson(route('api.groups.show', [$group->id]))
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'id' => $group->id,
                'title' => $group->title,
                'owner_id' => $group->owner->id,
            ]);
    }

    public function testShowDeleted()
    {
        $group = $this->createGroup();
        $group->delete();

        $this
            ->loginAs($group->owner->id)
            ->getJson(route('api.groups.show', [$group->id]))
            ->assertStatus(404)
            ->assertKeyNotExists('result.data.id');
    }

    public function testCreateByGuest()
    {
        $this
            ->postJson(route('api.groups.create'), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testCreate()
    {
        $user = $this->createUser();
        $group = $this->makeGroup();

        $data = [
            'title' => $group->title,
        ];

        $this
            ->loginAs($user->id)
            ->postJson(route('api.groups.create'), $data)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($data)
            ->assertDataKeysEqual([
                'owner_id' => $user->id,
            ]);
    }

    public function testCreateWithInvalidData()
    {
        $this->markTestIncomplete(); // @todo what are validation rules for Group
    }

    public function testUpdateByGuest()
    {
        $group = $this->createGroup();

        $this
            ->putJson(route('api.groups.update', [$group->id]), [
                'some' => 'data',
            ])
            ->assertStatus(401);
    }

    public function testUpdateByNotOwner()
    {
        $user = $this->createUser();
        $group = $this->createGroup();

        $this
            ->loginAs($user->id)
            ->putJson(route('api.groups.update', [$group->id]), [
                'some' => 'data',
            ])
            ->assertStatus(403);
    }

    public function testUpdate()
    {
        $group = $this->createGroup();

        $originalData = [
            'title' => $group->title,
        ];

        $modifiedData = array_merge($originalData, [
            'title' => $group->title . '-upd',
        ]);

        $this
            ->loginAs($group->owner->id)
            ->putJson(route('api.groups.update', [$group->id]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual($modifiedData);
    }

    public function testUpdateCannotChangeOwner()
    {
        $group = $this->createGroup();
        $anotherUser = $this->createUser();

        $originalData = [
            'title' => $group->title,
            'owner_id' => $group->owner->id,
        ];

        $modifiedData = array_merge($originalData, [
            'owner_id' => $anotherUser->id,
        ]);

        $this
            ->loginAs($group->owner->id)
            ->putJson(route('api.groups.update', [$group->id]), $modifiedData)
            ->assertStatus(200)
            ->assertKeyExists('result.data.id')
            ->assertDataKeysEqual([
                'owner_id' => $group->owner->id,
            ]);
    }

    public function testUpdateWithInvalidData()
    {
        $this->markTestIncomplete();
    }

    public function testUpdateDeleted()
    {
        $group = $this->createGroup();
        $group->delete();

        $this
            ->loginAs($group->owner->id)
            ->putJson(route('api.groups.update', [$group->id]), [
                'some' => 'data',
            ])
            ->assertStatus(404);
    }

    public function testDeleteByGuest()
    {
        $group = $this->createGroup();

        $this
            ->deleteJson(route('api.groups.delete', [$group->id]))
            ->assertStatus(401);
    }

    public function testDeleteByNotOwner()
    {
        $user = $this->createUser();
        $group = $this->createGroup();

        $this
            ->loginAs($user->id)
            ->deleteJson(route('api.groups.delete', [$group->id]))
            ->assertStatus(403);
    }

    public function testDelete()
    {
        $group = $this->createGroup();

        $this
            ->loginAs($group->owner->id)
            ->deleteJson(route('api.groups.delete', [$group->id]))
            ->assertStatus(200);

        $this
            ->loginAs($group->owner->id)
            ->getJson(route('api.groups.show', [$group->id]))
            ->assertStatus(404);
    }

    public function testDeleteDeleted()
    {
        $group = $this->createGroup();
        $group->delete();

        $this
            ->loginAs($group->owner->id)
            ->deleteJson(route('api.groups.delete', [$group->id]))
            ->assertStatus(404);
    }

}