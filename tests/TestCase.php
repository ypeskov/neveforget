<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Tests
 *
 * @method TestResponse getJson($uri, array $headers = [])
 * @method TestResponse postJson($uri, array $data = [], array $headers = [])
 * @method TestResponse putJson($uri, array $data = [], array $headers = [])
 * @method TestResponse deleteJson($uri, array $data = [], array $headers = [])
 *
 * @method TestResponse get($uri, array $headers = [])
 * @method TestResponse post($uri, array $data = [], array $headers = [])
 * @method TestResponse put($uri, array $data = [], array $headers = [])
 * @method TestResponse delete($uri, array $data = [], array $headers = [])
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Authorization token for the next request
     *
     * @var string|null
     */
    private $userAuthorizationToken = null;

    /**
     * @inheritdoc
     */
    protected function createTestResponse($response)
    {
        return TestResponse::fromBaseResponse($response);
    }

    /**
     * Create authorization token for the next request
     *
     * @param User|int $user
     * @return $this
     */
    public function loginAs($user)
    {
        $userId = $user instanceof User ? $user->id : $user;
        $this->userAuthorizationToken = \Auth::guard('api')->tokenById($userId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        if ($this->userAuthorizationToken) {
            $server = array_merge($this->transformHeadersToServerVars([
                'Authorization' => "Bearer {$this->userAuthorizationToken}",
            ]), $server);
        }

        $response = parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);

        // logout user after each request
        $this->userAuthorizationToken = null;
        if (\Auth::user()) {
            $token = \Auth::guard('api')->tokenById(\Auth::user()->id);
            \Auth::guard('api')->setToken($token)->logout();
        }

        return $response;
    }
}
