<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 6/10/16
 * Time: 12:16 PM
 */

namespace Tests;

use Illuminate\Foundation\Testing\TestResponse as LaravelTestResponse;
use PHPUnit\Framework\Assert as PHPUnit;

use PHPUnit_Framework_Assert;

class TestResponse extends LaravelTestResponse
{

    public function dump($debug = false)
    {
        if ($debug) {
            dump($this);
        }

        dump($this->decodeResponseJson());

        return $this;
    }

    public function dd($debug = false)
    {
        $this->dump($debug);
        die;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return array_get($this->decodeResponseJson(), $key);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return array_get($this->decodeResponseJson(), 'result.data.id');
    }

    /**
     * @return null|string
     */
    public function getErrors()
    {
        return array_get($this->decodeResponseJson(), 'result.errors');
    }

    /**
     * @param $key
     * @param string $message
     * @return $this
     */
    public function assertKeyExists($key, $message = null)
    {
        PHPUnit_Framework_Assert::assertTrue(array_has($this->decodeResponseJson(), $key), $message ?: "Key [{$key}] does not exist");

        return $this;
    }

    /**
     * @param $key
     * @param string $message
     * @return $this
     */
    public function assertKeyNotExists($key, $message = null)
    {
        PHPUnit_Framework_Assert::assertFalse(array_has($this->decodeResponseJson(), $key), $message ?: "Key [{$key}] does exist");

        return $this;
    }

    /**
     * @param $key
     * @param $expected
     * @param string $message
     * @return $this
     */
    public function assertKeyEquals($key, $expected, $message = null)
    {
        $actual = array_get($this->decodeResponseJson(), $key);

        $actualEncoded = json_encode($actual);
        $expectedEncoded = json_encode($expected);

        PHPUnit_Framework_Assert::assertEquals($expected, $actual, $message ?: "Key [{$key}]={$actualEncoded} is not equal to expected {$expectedEncoded}");

        return $this;
    }

    /**
     * @param array $keys
     * @param array $skipKeys
     * @return $this
     */
    public function assertKeysExist($keys, $skipKeys = [])
    {
        foreach ($keys as $key) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            $this->assertKeyExists($key);
        }

        return $this;
    }

    /**
     * @param array $keys
     * @param array $skipKeys
     * @return $this
     */
    public function assertKeysNotExist($keys, $skipKeys = [])
    {
        foreach ($keys as $key) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            $this->assertKeyNotExists($key);
        }

        return $this;
    }

    /**
     * @param array $keys
     * @param array $skipKeys
     * @return $this
     */
    public function assertErrorKeysExist($keys, $skipKeys = [])
    {
        foreach ($keys as $key) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            $this->assertKeyExists("result.errors.{$key}");
        }

        return $this;
    }

    /**
     * @param array $keys
     * @param array $skipKeys
     * @return $this
     */
    public function assertErrorKeysNotExist($keys, $skipKeys = [])
    {
        foreach ($keys as $key) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            $this->assertKeyNotExists("result.errors.{$key}");
        }

        return $this;
    }

    /**
     * @param array $keysExpectedValuesMap
     * @param array $skipKeys
     * @return $this
     */
    public function assertKeysEqual($keysExpectedValuesMap, $skipKeys = [])
    {
        foreach ($keysExpectedValuesMap as $key => $expected) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            $this->assertKeyEquals($key, $expected);
        }

        return $this;
    }

    /**
     * @param array $keysExpectedValuesMap
     * @param array $skipKeys
     * @return $this
     */
    public function assertDataKeysEqual($keysExpectedValuesMap, $skipKeys = [])
    {
        $this->assertNestedKeysEqual('result.data', $keysExpectedValuesMap, $skipKeys);

        return $this;
    }

    /**
     * @param string $rootKeyPath
     * @param array $keysExpectedValuesMap
     * @param array $skipKeys
     * @return $this
     */
    public function assertNestedKeysEqual($rootKeyPath, $keysExpectedValuesMap, $skipKeys = [])
    {
        foreach ($keysExpectedValuesMap as $key => $expected) {
            if (in_array($key, $skipKeys)) {
                continue;
            }

            if (is_array($expected)) {
                $this->assertNestedKeysEqual("{$rootKeyPath}.{$key}", $expected, $skipKeys);
                continue;
            }

            $this->assertKeyEquals("{$rootKeyPath}.{$key}", $expected, "Key {$rootKeyPath}.{$key} has unexpected value");
        }

        return $this;
    }

    /**
     * @param $key
     * @param $expected
     * @param null $message
     * @return $this
     */
    public function assertKeyChildrenCountEquals($key, $expected, $message = null)
    {
        $actual = count(array_get($this->decodeResponseJson(), $key, []));
        PHPUnit_Framework_Assert::assertEquals($expected, $actual, $message ?: "Failed asserting that [{$key}] contains expected [{$expected}] elements ([{$actual}] found)");

        return $this;
    }

    public function assertStatus($status, $message = null)
    {
        $actual = $this->getStatusCode();
        $message = "Expected status code {$status} but received {$actual}.";

        if ($this->getErrors()) {
            $message .= PHP_EOL . json_encode($this->getErrors(), JSON_PRETTY_PRINT);
        }

        PHPUnit::assertTrue($actual === $status, $message);

        return $this;
    }


}