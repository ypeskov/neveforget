<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 07.8.16
 * Time: 18:01
 */

namespace Tests\Traits;

use App\Models\Contact;
use App\Models\Person;

/**
 * Class ContactsTrait
 * @package Tests\Utils
 *
 * @method Person createPerson
 */
trait ContactsTrait
{
    /**
     * @param null|int $personId
     * @param array $attributes
     * @return Contact
     */
    protected function createContact($personId = null, $attributes = [])
    {
        if (is_null($personId)) {
            $personId = $this->createPerson()->id;
        }

        $attributes = array_merge($attributes, [
            'person_id' => $personId,
        ]);

        return factory(Contact::class)
            ->create($attributes);
    }

    /**
     * @param null $personId
     * @param array $attributesOriginal
     * @param array $attributesOverridden
     * @return Contact
     * @throws \Exception
     */
    protected function createContactWithOverridden($personId = null, $attributesOriginal = [], $attributesOverridden = [])
    {
        if ($personId != null && !Person::find($personId)->is_synced) {
            throw new \Exception('Only synced persons can have overridden contacts');
        }

        $original = $this->createContact($personId, $attributesOriginal);

        /** @var Contact $overridden */
        $overridden = $original->replicate();
        $overridden
            ->fill($attributesOverridden)
            ->save();

        $original->overriddenContact()->associate($overridden);
        $original->save();

        return $original;
    }

    /**
     * @param null|int $personId
     * @param array $attributes
     * @return Contact
     */
    protected function makeContact($personId = null, $attributes = [])
    {
        if ($personId) {
            $attributes = array_merge($attributes, [
                'person_id' => $personId,
            ]);
        }

        return factory(Contact::class)
            ->make($attributes);
    }
}