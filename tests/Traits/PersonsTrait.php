<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 07.8.16
 * Time: 18:01
 */

namespace Tests\Traits;

use App\Models\Person;
use App\Models\Social;
use App\User;

/**
 * Class PersonsTrait
 * @package Tests\Utils
 *
 * @method User createUser
 * @method Social createSocial
 */
trait PersonsTrait
{
    /**
     * Make & Save
     *
     * @param int|null $ownerId
     * @param array $attributes
     * @return Person
     */
    protected function createPerson($ownerId = null, $attributes = [])
    {
        $attributes = array_merge($attributes, [
            'owner_id' => $ownerId ?: $this->createUser()->id,
        ]);

        return factory(Person::class)
            ->create($attributes);
    }

    /**
     * Make (Don't Save)
     *
     * @param int|null $ownerId
     * @param array $attributes
     * @return Person
     */
    protected function makePerson($ownerId = null, $attributes = [])
    {
        if ($ownerId) {
            $attributes = array_merge($attributes, [
                'owner_id' => $ownerId,
            ]);
        }

        return factory(Person::class)
            ->make($attributes);
    }

    /**
     * Make & Save
     *
     * @param int|null $ownerId
     * @param null $socialSourceId
     * @param array $attributes
     * @return Person
     */
    protected function createSyncedPerson($ownerId = null, $socialSourceId = null, $attributes = [])
    {
        $attributes = array_merge($attributes, [
            'owner_id' => $ownerId ?: $this->createUser()->id,
            'social_source_id' => $socialSourceId ?? $this->createSocial()->id
        ]);

        return factory(Person::class)
            ->create($attributes);
    }

    /**
     * Make (Don't Save)
     *
     * @param int|null $ownerId
     * @param null $socialSourceId
     * @param array $attributes
     * @return Person
     */
    protected function makeSyncedPerson($ownerId = null, $socialSourceId, $attributes = [])
    {
        if ($ownerId) {
            $attributes = array_merge($attributes, [
                'owner_id' => $ownerId,
            ]);
        }

        $attributes = array_merge($attributes, [
            'social_source_id' => $socialSourceId,
        ]);

        return factory(Person::class)
            ->make($attributes);
    }
}