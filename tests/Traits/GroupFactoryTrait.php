<?php

namespace Tests\Traits;

use App\Models\Group;

trait GroupFactoryTrait
{
    /**
     * Just make
     *
     * @param null $ownerId
     * @param array $attributes
     * @return Group
     */
    public function makeGroup($ownerId = null, $attributes = [])
    {
        if ($ownerId) {
            $attributes['owner_id'] = $ownerId;
        }

        return factory(Group::class)->make($attributes);
    }

    /**
     * Make & Save
     *
     * @param null $ownerId
     * @param array $attributes
     * @return Group
     */
    public function createGroup($ownerId = null, $attributes = [])
    {
        if ($ownerId) {
            $attributes['owner_id'] = $ownerId;
        }

        return factory(Group::class)->create($attributes);
    }
}