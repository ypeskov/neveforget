<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * DateType: 07.8.16
 * Time: 18:01
 */

namespace Tests\Traits;

use App\Models\DateType;
use App\Models\Person;

/**
 * Class DateTypesTrait
 * @package Tests\Utils
 *
 * @method Person createPerson
 */
trait DateTypesTrait
{
    /**
     * @param array $attributes
     * @return DateType
     */
    protected function createDateType($attributes = [])
    {
        return factory(DateType::class)
            ->create($attributes);
    }

    /**
     * @param array $attributes
     * @return DateType
     */
    protected function makeDateType($attributes = [])
    {
        return factory(DateType::class)
            ->make($attributes);
    }
}