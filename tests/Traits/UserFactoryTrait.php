<?php

namespace Tests\Traits;

use App\User;

trait UserFactoryTrait
{
    /**
     * Just make
     *
     * @param array $attributes
     * @return User
     */
    public function makeUser($attributes = [])
    {
        return factory(User::class)->make($attributes);
    }

    /**
     * Make & Save
     *
     * @param array $attributes
     * @return User
     */
    public function createUser($attributes = [])
    {
        return factory(User::class)->create($attributes);
    }
}