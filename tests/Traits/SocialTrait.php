<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 11.9.16
 * Time: 17:09
 */

namespace Tests\Traits;

use App\Models\Social;
use App\User;

/**
 * Class SocialTrait
 * @package Tests\Utils
 *
 * @method User createUser
 */
trait SocialTrait
{
    /**
     * Make & Save
     *
     * @param int|null $ownerId
     * @param array $attributes
     * @return Social
     */
    protected function createSocial($ownerId = null, $attributes = [])
    {
        $attributes = array_merge($attributes, [
            'owner_id' => $ownerId ?: $this->createUser()->id,
        ]);

        return factory(Social::class)
            ->create($attributes);
    }

    /**
     * Make (Don't Save)
     *
     * @param int|null $ownerId
     * @param array $attributes
     * @return Social
     */
    protected function makeSocial($ownerId = null, $attributes = [])
    {
        if ($ownerId) {
            $attributes = array_merge($attributes, [
                'owner_id' => $ownerId,
            ]);
        }

        return factory(Social::class)
            ->make($attributes);
    }

}