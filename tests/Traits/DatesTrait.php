<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 07.8.16
 * Time: 18:01
 */

namespace Tests\Traits;

use App\Models\Date;
use App\Models\Person;

/**
 * Class DatesTrait
 * @package Tests\Utils
 *
 * @method Person createPerson
 */
trait DatesTrait
{
    /**
     * @param null|int $personId
     * @param array $attributes
     * @return Date
     */
    protected function createDate($personId = null, $attributes = [])
    {
        if (is_null($personId)) {
            $personId = $this->createPerson()->id;
        }

        $attributes = array_merge($attributes, [
            'person_id' => $personId,
        ]);

        return factory(Date::class)
            ->create($attributes);
    }

    /**
     * @param null $personId
     * @param array $attributesOriginal
     * @param array $attributesOverridden
     * @return Date
     * @throws \Exception
     */
    protected function createDateWithOverridden($personId = null, $attributesOriginal = [], $attributesOverridden = [])
    {
        if ($personId != null && !Person::find($personId)->is_synced) {
            throw new \Exception('Only synced persons can have overridden dates');
        }

        $original = $this->createDate($personId, $attributesOriginal);

        /** @var Date $overridden */
        $overridden = $original->replicate();
        $overridden
            ->fill($attributesOverridden)
            ->save();

        $original->overriddenDate()->associate($overridden);
        $original->save();

        return $original;
    }

    /**
     * @param null|int $personId
     * @param array $attributes
     * @return Date
     */
    protected function makeDate($personId = null, $attributes = [])
    {
        if ($personId) {
            $attributes = array_merge($attributes, [
                'person_id' => $personId,
            ]);
        }

        return factory(Date::class)
            ->make($attributes);
    }
}