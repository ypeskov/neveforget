<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 08.8.16
 * Time: 21:42
 */

namespace App\Repositories\Metaperson;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface MetapersonRepositoryInterface
{
    public function createMetapersonFromRequest(Request $request);

    public function updateMetapersonFromRequest($id, Request $request);

    public function deleteMetaperson($id);

    public function getUserMetapersons(User $user);

    public function modelBelongsToUser(Model $model, $userId);

    public function getUserMetaperson($metapersonId, $skipOwnershipCheck = false);

    public function all();
}