<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 08.8.16
 * Time: 21:42
 */

namespace App\Repositories\Metaperson;

use App\Models\Group;
use App\Models\Person;
use App\Repositories\AbstractRepository;
use App\Repositories\Exception\NotFoundRepositoryException;
use App\Repositories\Exception\RepositoryException;
use App\Repositories\Person\PersonRepository;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Container\Container as App;

/**
 * Class MetapersonRepository
 * @package App\Repositories\Metaperson
 */
class MetapersonRepository extends AbstractRepository implements MetapersonRepositoryInterface
{
    protected $findable = [
        'id',
        'is_local_type',
        'social_source_id',
        'group',
    ];
    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * MetapersonRepository constructor.
     * @param App $app
     * @param PersonRepository $personRepository
     */
    public function __construct(App $app, PersonRepository $personRepository)
    {
        parent::__construct($app);
        $this->personRepository = $personRepository;
    }

    /**
     * @return mixed
     */
    public function model()
    {
        return Person::class;
    }

    public function createMetaperson($data)
    {
        return $this->personRepository->createPerson($data);
    }

    /**
     * @param Request $request
     * @return Model
     */
    public function createMetapersonFromRequest(Request $request)
    {
        return $this->personRepository->createPersonFromRequest($request);
    }

    public function updateMetaperson($id, $data)
    {
        return $this->personRepository->updatePerson($id, $data);
    }

    /**
     * @param $id
     * @param Request $request
     * @return Model
     */
    public function updateMetapersonFromRequest($id, Request $request)
    {
        /**
         * @var Person $model
         */
        $model = $this->getById($id);

        return $this->personRepository->updatePersonFromRequest($model->id, $request);
    }

    /**
     * @param $id
     * @throws RepositoryException
     */
    public function deleteMetaperson($id)
    {
        /**
         * @var Person $model
         */
        $model = $this->getById($id);

        foreach ($model->mergedPersons as $mergedPerson) {
            // social contacts will be unmerged and hidden
            if ($mergedPerson->social_source_id) {
                $this->unmergePerson($mergedPerson->id);
            }

            $this->personRepository->deletePerson($mergedPerson->id);
        }

        $this->personRepository->deletePerson($id);
    }

    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUserMetapersons(User $user)
    {
        return $this
            ->getAllVisibleRecords()
            ->where('owner_id', $user->id)
            ->with('contacts')
            ->get();
    }

    /**
     * @param Model|Person $model
     * @param $userId
     * @return bool
     */
    public function modelBelongsToUser(Model $model, $userId)
    {
        if ((int)$userId !== (int)$model->owner_id) {
            return false;
        }

        return true;
    }

    /**
     * @param $metapersonId
     * @param bool $skipOwnershipCheck
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function getUserMetaperson($metapersonId, $skipOwnershipCheck = false)
    {
        return $this->getById($metapersonId, $skipOwnershipCheck);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this
            ->getAllVisibleRecords()
            ->get();
    }

    /**
     * @param Collection $collection
     * @param null $request
     * @return Collection
     * @throws \Exception
     */
    public function filterByRequest(Collection $collection, $request = null)
    {
        if (is_null($request)) {
            $request = \Request::all();
        }

        if ($request instanceof \Request) {
            $request = $request->all();
        }

        if (!is_array($request)) {
            throw new \Exception('Invalid request type');
        }

        $request = array_only($request, $this->findable);

        return $collection->filter(function (Person $person) use ($request) {
            foreach ($request as $filter => $criteria) {
                $isCustom = in_array($filter, ['is_local_type', 'group']);

                if ($filter == 'is_local_type' && is_null($person->social_source_id) != $criteria) {
                    return false;
                }

                if ($filter == 'group' && $person->groups()->where('id', $criteria)->count() == 0) {
                    return false;
                }

                if ($isCustom) {
                    continue;
                }

                if (is_scalar($criteria) && $person->{$filter} != $criteria) {
                    return false;
                }

                if (is_array($criteria) && !in_array($person->{$filter}, $criteria)) {
                    return false;
                }
            }

            return true;
        })->values();
    }

    public function getById($id, $skipOwnershipCheck = false)
    {
        /**
         * @var Person $model
         */
        $model = parent::getById($id, $skipOwnershipCheck);

        if ($model->parent_person_id) {
            throw new NotFoundRepositoryException();
        }

        return $model;
    }

    public function mergePerson($metapersonId, $personId)
    {
        /**
         * @var Person $person
         * @var Person $metaperson
         */

        $person = $this->personRepository->getById($personId);
        $metaperson = $this->getById($metapersonId);

        $person->parent_person_id = $metaperson->id;
    }

    /**
     * Split Person to separated Metaperson
     *
     * @param $personId
     */
    public function unmergePerson($personId)
    {
        /**
         * @var Person $model
         */
        $model = $this->personRepository->getById($personId);
        $model->parent_person_id = null;
        $model->save();
    }

    public function mergePersons($ids)
    {
        // @fixme choose the most complete local person
        // @fixme check permissions
        $localPerson = $this->model
            ->whereIn('id', $ids)
            ->whereNull('social_source_id')
            ->first();

        if (!$localPerson) {
            // @fixme compile a complete person from $persons
            // @fixme should notes contain notes from all persons?
            /**
             * @var Person $syncedPerson
             */
            $syncedPerson = $this->model
                ->whereIn('id', $ids)
                ->whereNotNull('social_source_id')
                ->first();

            $profileData = array_only($syncedPerson->getAttributes(), [
                'first_name', 'last_name', 'nick_name', 'notes', 'job_title', 'company',
                'owner_id' // @todo do not lose this
            ]);

            $localPerson = $this->model->create($profileData);
        }

        /**
         * @var Person[] $models
         */
        $models = $this->model
            ->whereIn('id', $ids)
            ->where('id', '!=', $localPerson->id)
            ->get();

        \DB::beginTransaction();
        foreach ($models as $model) {
            $model->parent_person_id = $localPerson->id;
            $model->save();
        }
        \DB::commit();

        return $localPerson;
    }

    public function getMetapersonPersons($id)
    {
        $user = \Auth::getUser();

        return $this->model
            ->where('owner_id', $user->id)
            ->where('parent_person_id', $id)
            ->orWhere('id', $id)
            ->get();
    }

    /**
     * Use this method to request all records.
     * Do not request them elsewhere.
     * Add more conditions if needed.
     *
     * @return \Illuminate\Database\Eloquent\Builder|Model|\Illuminate\Database\Query\Builder
     */
    protected function getAllRecords()
    {
        return $this->model
            ->whereNull('parent_person_id');
    }

    /**
     * @return $this
     */
    protected function getAllVisibleRecords()
    {
        return $this
            ->getAllRecords()
            ->where('is_hidden', '!=', true);
    }

    /**
     * @param int $metapersonId
     * @param array $groups
     */
    public function setGroups($metapersonId, $groups)
    {
        $metaperson = $this->getById($metapersonId);
        $metaperson->groups()->detach();

        $groupModels = [];

        foreach ($groups as $group) {
            $title = $group['title'];

            /** @var Group $groupModel */
            $groupModels[] = Group::firstOrCreate([
                'slug' => str_slug($title),
                'owner_id' => \Auth::getUser()->id,
            ], [
                'title' => $title,
            ])->id;
        }

        $metaperson->groups()->sync($groupModels);
    }
}