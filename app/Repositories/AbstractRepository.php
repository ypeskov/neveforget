<?php

/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 04.01.16
 * Time: 17:38
 */
namespace App\Repositories;

use App\Repositories\Exception\NotFoundRepositoryException;
use App\Repositories\Exception\RepositoryException;
use App\Repositories\Exception\UnauthorizedRepositoryException;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;

abstract class AbstractRepository implements RepositoryInterface
{
    private $app;

    /**
     * @var Model|EloquentBuilder|QueryBuilder
     */
    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;

        $this->makeModel();
    }

    abstract public function model();

    abstract public function modelBelongsToUser(Model $model, $userId);

    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if ( !$model instanceof Model ) {
            throw new RepositoryException(
                "Class {$this->model()} must be an instance of Illuminate\\Database\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Get model by id with existence and ownership check.
     *
     * @param $id
     * @param bool $skipOwnershipCheck
     * @return Model|EloquentBuilder|QueryBuilder
     * @throws NotFoundRepositoryException
     * @throws UnauthorizedRepositoryException
     */
    public function getById($id, $skipOwnershipCheck = false)
    {
        $model = $this->model->find($id);

        $this->failIfNotFound($model);

        if (!$skipOwnershipCheck) {
            $this->failIfNotAuthorized($model);
        }

        return $model;
    }

    /**
     * @param Model|EloquentBuilder|QueryBuilder|null $model
     * @throws NotFoundRepositoryException
     */
    protected function failIfNotFound($model)
    {
        if (!is_null($model)) {
            return;
        }

        throw new NotFoundRepositoryException();
    }

    /**
     * @param Model|EloquentBuilder|QueryBuilder $model
     * @throws UnauthorizedRepositoryException
     */
    protected function failIfNotAuthorized($model)
    {
        if ($this->modelBelongsToUser($model, \Auth::user()->id)) {
            return;
        }

        throw new UnauthorizedRepositoryException();
    }

    /**
     * @param Request|array $request
     * @return array
     */
    protected function extractDataFromRequest($request)
    {
        $requestData = $request instanceof Request
            ? $request->all()
            : $request;

        $data = array_only($requestData, $this->model->getFillable());

        return $data;
    }
}