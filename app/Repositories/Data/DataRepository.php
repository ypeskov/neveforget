<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 16.12.16
 * Time: 21:45
 */

namespace App\Repositories\Data;

use App\Models\Contact;
use DB;
use Illuminate\Support\Collection;

class DataRepository implements DataRepositoryInterface
{
    /**
     * Returns all contacts that belong to the user
     *
     * @param $user
     * @return array|Collection
     */
    public function getUserContacts($user)
    {
        $persons = DB::table('persons')
            ->where('owner_id', $user->id)
            ->pluck('id');

        return Contact::whereIn('person_id', $persons)->get();
    }
}