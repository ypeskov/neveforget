<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 16.12.16
 * Time: 21:44
 */
namespace App\Repositories\Data;

use Illuminate\Support\Collection;

interface DataRepositoryInterface
{
    /**
     * 
     * @user   Auth::user()
     * @return Collection
     */
    public function getUserContacts($user);
}