<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 04.01.16
 * Time: 20:36
 */

namespace App\Repositories\Event;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Event;

interface EventRepositoryInterface
{
    public function createEventFromRequest(Request $request, $userId);

    public function updateEventFromRequest(Request $request);

    public function deleteEvent($id);

    public function getUserEvents(User $user);

    public function reduceDateToHour(Collection $eventCollection);

    public function eventBelongsToUser(Event $event, $userId);

    public function getUserEvent($eventId, $skipOwnershipCheck = false);

    public function addDateAndTime(Event $event);

    public function setStatus($eventId, $newStatus);

    public function toggleStatus($eventId);

    public function all();

    public function filterByYear(Collection $eventsCollection, $year);

    public function filterByRequest(Collection $eventsCollection, $request = null);
}