<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 04.01.16
 * Time: 18:27
 */

namespace App\Repositories\Event;

use App\Errors\ErrorCode;
use App\Models\Event;
use App\Repositories\AbstractRepository;
use App\Repositories\Exception\RepositoryException;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \Auth;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Exception\UnauthorizedRepositoryException;
use App\Repositories\Exception\NotFoundRepositoryException;
use Log;
use DB;

class EventRepository extends AbstractRepository implements EventRepositoryInterface
{
    protected $findable = [
        'activeDate',
        'period',
        'is_active',
        'event_types_id',
    ];

    public function model()
    {
        return 'App\Models\Event';
    }

    public function createEventFromRequest(Request $request, $userId, $eventType=Event::TYPE_EMAIL)
    {
        $event                  = $request->toArray();
        $event['user_id']       = $userId;
        $event['event_types_id']= $eventType;
        $event['is_active']     = array_get($event, 'is_active', true);

        return $this->model->create($event);
    }

    public function updateEventFromRequest(Request $request)
    {
        $foundEvent = Event::find($request->id);

        if ( (int) $foundEvent->user_id !== (int) Auth::user()->id ) {
            throw new UnauthorizedRepositoryException();
        }

        if ( $foundEvent === null ) {
            throw new NotFoundRepositoryException();
        }

        $event = $this->prepareEventData($request);
        $event['id'] = $request->id;

        foreach($event as $attr => $val) {
            $foundEvent->{$attr} = $val;
        }

        $foundEvent->save();

        return true;
    }

    public function deleteEvent($id)
    {
        $event = $this->model->find($id);

        if ( !$event ) {
            throw new NotFoundRepositoryException();
        }

        if ( !$event->delete() ) {
            throw new RepositoryException(ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
        }

        return true;
    }

    public function reduceDateToHour(Collection $eventCollection)
    {
        foreach($eventCollection as $event) {
            $time = strtotime($event->datetime_to_send);
            $event->datetime_to_send = date('Y-m-d', $time);
        }

        return $eventCollection;
    }

    public function getUserEvents(User $user)
    {
        return $this->model
            ->where('user_id', $user->id)
            ->orderBy('datetime_to_send')
            ->get();
    }

    public function eventBelongsToUser(Event $event, $userId)
    {
        if ( (int)$userId !== (int)$event->user_id ) {
            return false;
        }

        return true;
    }

    public function getUserEvent($eventId, $skipOwnershipCheck = false)
    {
        $event  = $this->model->find($eventId);

        if ( $event === null ) {
            throw new NotFoundRepositoryException();
        }

        if (!$skipOwnershipCheck && !$this->eventBelongsToUser($event, \Auth::user()->id)) {
            throw new UnauthorizedRepositoryException();
        }

        return $event;
    }

    public function addDateAndTime(Event $event)
    {
        $dateTimeParts = explode(" ", $event->datetime_to_send);
        $event->desired_date = $dateTimeParts[0];
        $event->desired_time = $dateTimeParts[1];

        return $event;
    }

    public function setStatus($eventId, $newStatus)
    {
        $event = Event::find($eventId);
        $event->is_active = $newStatus;

        return $this;
    }

    public function toggleStatus($eventId)
    {
        $event = Event::find($eventId);
        $event->is_active = !(bool) $event->is_active;
        $event->save();

        return $this;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getEventsInCurrentHour(string $currentDate, string $currentHour)
    {
        $events =
            DB::select("select * from events where (:curDate = date(events.datetime_to_send)) and ( :curHour = hour(events.datetime_to_send))",
                [
                    'curDate'  => $currentDate,
                    'curHour'  => $currentHour,
                ]);

        return $events;
    }
    
    public function getEventsByDate(string $dateStr)
    {
        $dateTime = date("Y-m-d H", strtotime($dateStr));
        $events = $this->model
            ->whereDate("datetime_to_send", "=", $dateTime)
            ->orderBy("datetime_to_send")
            ->get();

        return $events;
    }

    public function filterByYear(Collection $eventsCollection, $year)
    {
        return $eventsCollection->filter(function (Event $item) use ($year) {
            return $item->datetime_to_send->year == $year;
        });
    }

    /**
     * @param Collection $eventsCollection
     * @param null $request
     * @return Collection
     * @throws \Exception
     */
    public function filterByRequest(Collection $eventsCollection, $request = null)
    {
        if (is_null($request)) {
            $request = \Request::all();
        }

        if ($request instanceof Request) {
            $request = $request->all();
        }

        if (!is_array($request)) {
            throw new \Exception('Invalid request type');
        }

        $request = array_only($request, $this->findable);

        $requestDate = json_decode($request['activeDate']);

        return $eventsCollection->filter(function (Event $event) use ($request, $requestDate) {
            //put from GMT to appropriate time zone
            $event->datetime_to_send  =
                (new Carbon($event->datetime_to_send, 'GMT'))
                    ->setTimezone($event->timezone)
                    ->toDateTimeString();
            $eventDate = $event->datetime_to_send;

            switch ($request['period']) {
                case    'day':
                    if ( !$this->isDateSame($eventDate->format('Y-m-d'), $requestDate) ) {
                        return false;
                    }
                    break;
                case    'week':
                    if ( !$this->isWeekSame($eventDate, $requestDate) ) {
                        return false;
                    }
                    break;
                case    'month':
                    if ( !$this->isMonthSame($eventDate, $requestDate) ) {
                        return false;
                    }
                    break;
                case    'year':
                    if ( $event->datetime_to_send->year != $requestDate->year) {
                        return false;
                    }
                    break;
                default:
                    throw new \Exception('Invalid filter period');
            }

            return true;
        })->values();
    }

    /**
     * If the dates are the same (without hours and minutes) returns true.
     *
     * @param $eventDate    Carbon date
     * @param $requestDate  Carbon date
     * @return bool
     */
    private function isDateSame($eventDate, $requestDate)
    {
        $requestDate = $requestDate->year
            . '-'
            . $requestDate->month
            . '-'
            . $requestDate->day;

        if ( strtotime($eventDate) !== strtotime($requestDate) ) {
            return false;
        }

        return true;
    }

    private function isMonthSame($eventDate, $requestDate)
    {
        $beginOfMonth = Carbon::create($requestDate->year, $requestDate->month, $requestDate->day)->startOfMonth();
        $endOfMonth     = $beginOfMonth->copy()->endOfMonth();

        if ( $eventDate->gte($beginOfMonth) and $eventDate->lte($endOfMonth) ) {
            return true;
        }

        return false;
    }

    /**
     * If the event date is within $requestDate week returns true, otherwise false
     *
     * @param $eventDate  Carbon date
     * @param $requestDate Carbon date
     * @return bool
     */
    private function isWeekSame($eventDate, $requestDate)
    {
        $beginOfWeek = Carbon::create($requestDate->year, $requestDate->month, $requestDate->day)->startOfWeek();
        $endOfWeek  = $beginOfWeek->copy()->endOfWeek();

        if ( $eventDate->gte($beginOfWeek) and $eventDate->lte($endOfWeek) ) {
            return true;
        }

        return false;
    }

    /**
     * @param Model $model
     * @param $userId
     * @return bool
     */
    public function modelBelongsToUser(Model $model, $userId)
    {
        return $this->eventBelongsToUser($model, $userId);
    }
}