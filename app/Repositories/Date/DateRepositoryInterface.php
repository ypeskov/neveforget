<?php

namespace App\Repositories\Date;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface DateRepositoryInterface
{
    public function createDateFromRequest($personId, Request $request);

    public function updateDateFromRequest($personId, $dateId, Request $request);

    public function deleteDate($personId, $dateId);

    public function getPersonDates($personId);

    public function modelBelongsToUser(Model $model, $userId);

    public function getUserDate($personId, $dateId, $skipOwnershipCheck = false);

    public function all();

    public function filterByRequest(Collection $collection, $request = null);
}