<?php

namespace App\Repositories\Date;

use App\Errors\ErrorCode;
use App\Models\DateType;
use App\Models\Meta\Date as Metadate;
use App\Models\Person;
use App\Repositories\AbstractRepository;
use App\Repositories\Exception\NotFoundRepositoryException;
use App\Repositories\Exception\RepositoryException;
use App\Repositories\Person\PersonRepository;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class DateRepository extends AbstractRepository implements DateRepositoryInterface
{
    protected $findable = [
        'date',
        'date_type',
        'date_type_id',
    ];

    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * DateRepository constructor.
     * @param App $app
     * @param PersonRepository $personRepository
     */
    public function __construct(App $app, PersonRepository $personRepository)
    {
        parent::__construct($app);
        $this->personRepository = $personRepository;
    }

    public function model()
    {
        return Metadate::class;
    }

    public function createDate($personId, $data)
    {
        $person = $this->personRepository->getById($personId);
        $data['person_id'] = $person->id;

        if (!isset($data['date_type_id'])) {
            $data['date_type_id'] = DateType::updateOrCreate([
                'owner_id' => \Auth::user()->id,
                'title' => array_get($data, 'date_type', ''),
            ])->id;
            // @todo date type (custom)
        }

        return $this->model->create($data);
    }

    public function createDateFromRequest($personId, Request $request)
    {
        $data = $this->extractDataFromRequest($request);

        return $this->createDate($personId, $data);
    }

    public function updateDate($personId, $dateId, $data)
    {
        /** @var Metadate $model */
        $model = $this->getById($dateId);
        $this->failIfPersonDiffers($model, $personId);

        if (!$model->person->is_synced) {
            $model->update($data);
        } else {
            $overriddenModel = $model->overriddenDate;

            if (!$overriddenModel) {
                /** @var Metadate $overriddenModel */
                $overriddenModel = $model->replicate();
                $overriddenModel->save();
                $model->overriddenDate()->associate($overriddenModel);
                $model->save();
            }

            $overriddenModel->update($data);
        }

        return $model;
    }

    public function updateDateFromRequest($personId, $dateId, Request $request)
    {
        $data = $this->extractDataFromRequest($request);

        return $this->updateDate($personId, $dateId, $data);
    }

    public function deleteDate($personId, $dateId)
    {
        $model = $this->getById($dateId);
        $this->failIfPersonDiffers($model, $personId);

        // @fixme check if is imported

        if (!$model->delete()) {
            throw new RepositoryException(ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
        }
    }

    public function getPersonDates($personId)
    {
        return $this->model
            ->where('person_id', $personId)
            ->get();
    }

    public function getMetapersonDates($metapersonId)
    {
        $persons = Person::where('parent_person_id', $metapersonId)
            ->orWhere('id', $metapersonId)
            ->get();

        return $this->model
            ->whereIn('person_id', $persons->pluck('id'))
            ->whereNull('overridden_date_id')
            ->get();
    }

    /**
     * @param Model|Metadate $model
     * @param $userId
     * @return bool
     */
    public function modelBelongsToUser(Model $model, $userId)
    {
        if ((int)$userId !== (int)$model->person->owner_id) { // @fixme is it ok to get Person without PersonRepository?
            return false;
        }

        return true;
    }

    public function getUserDate($personId, $dateId, $skipOwnershipCheck = false)
    {
        $model = $this->getById($dateId, $skipOwnershipCheck);
        $this->failIfPersonDiffers($model, $personId);

        return $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param Collection $collection
     * @param null $request
     * @return Collection
     * @throws \Exception
     */
    public function filterByRequest(Collection $collection, $request = null)
    {
        if (is_null($request)) {
            $request = \Request::all();
        }

        if ($request instanceof \Request) {
            $request = $request->all();
        }

        if (!is_array($request)) {
            throw new \Exception('Invalid request type');
        }

        $request = array_only($request, $this->findable);

        return $collection->filter(function (Metadate $model) use ($request) {
            foreach ($request as $field => $value) {

                if ($field == 'date_type') {
                    // @todo add date_type filter

                    continue;
                }

                if ($model->{$field} != $value) {
                    return false;
                }
            }

            return true;
        })->values();
    }

    /**
     * @param Model|Date $model
     * @param $personId
     * @throws NotFoundRepositoryException
     */
    protected function failIfPersonDiffers(Model $model, $personId)
    {
        if ((int)$model->person_id == (int)$personId) {
            return;
        }

        if ((int)$model->person->parent_person_id == (int)$personId) {
            return;
        }

        throw new NotFoundRepositoryException();
    }
}