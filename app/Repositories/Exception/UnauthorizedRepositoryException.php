<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 08.01.16
 * Time: 17:24
 */

namespace App\Repositories\Exception;


class UnauthorizedRepositoryException extends RepositoryException
{
    const ACCESS_DENIED     = "Access denied";

    public function __construct($message="", $code=0, Exception $previous=null)
    {
        parent::__construct($message, $code, $previous);

        $this->message  = self::ACCESS_DENIED;
    }
}