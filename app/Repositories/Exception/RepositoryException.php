<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 04.01.16
 * Time: 18:48
 */

namespace App\Repositories\Exception;


class RepositoryException extends \Exception
{

}