<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 08.01.16
 * Time: 18:52
 */

namespace App\Repositories\Exception;

use App\Repositories\Exception\RepositoryException;

class NotFoundRepositoryException extends RepositoryException
{
    protected $message = "Not found";
}