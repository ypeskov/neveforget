<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 08.8.16
 * Time: 21:42
 */

namespace App\Repositories\Person;

use App\Errors\ErrorCode;
use App\Exceptions\CustomValidationException;
use App\Models\Person;
use App\Repositories\AbstractRepository;
use App\Repositories\Exception\RepositoryException;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class PersonRepository
 * @package App\Repositories\Person
 */
class PersonRepository extends AbstractRepository implements PersonRepositoryInterface
{
    protected $findable = [];

    /**
     * @return mixed
     */
    public function model()
    {
        return Person::class;
    }

    /**
     * @param $data
     * @return Person
     */
    public function createPerson($data)
    {
        $data['owner_id'] = \Auth::user()->id;

        $this->validate($data);

        return $this->model->create($data);
    }

    /**
     * @param Request $request
     * @return Person
     */
    public function createPersonFromRequest(Request $request)
    {
        $data = $this->extractDataFromRequest($request);

        return $this->createPerson($data);
    }

    public function updatePerson($id, $data)
    {
        $model = $this->getById($id);

        $this->validate($data, true);
        $model->update($data);

        return $model;
    }

    /**
     * @param Request $request
     * @return Person
     */
    public function updatePersonFromRequest($id, Request $request)
    {
        $data = $this->extractDataFromRequest($request);

        return $this->updatePerson($id, $data);
    }

    /**
     * @param $id
     * @throws RepositoryException
     */
    public function deletePerson($id)
    {
        /**
         * @var Person $model
         */
        $model = $this->getById($id);

        // imported persons can't be deleted
        if ($model->social_source_id) {
            $model->is_hidden = true;
            $model->save();
            return;
        }

        if (!$model->delete()) {
            throw new RepositoryException(ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUserPersons(User $user)
    {
        return $this->model
            ->where('owner_id', $user->id)
            ->get();
    }

    /**
     * @param Model|Person $model
     * @param $userId
     * @return bool
     */
    public function modelBelongsToUser(Model $model, $userId)
    {
        if ((int)$userId !== (int)$model->owner_id) {
            return false;
        }

        return true;
    }

    /**
     * @param $personId
     * @param bool $skipOwnershipCheck
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function getUserPerson($personId, $skipOwnershipCheck = false)
    {
        return $this->getById($personId, $skipOwnershipCheck);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function filterByRequest(Collection $collection)
    {
        return $collection;
    }

    protected function validate($data, $isUpdate = false)
    {
        if ($data instanceof Request) {
            $data = $data->all();
        }

        $validator = \Validator::make($data, [
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'nick_name' => 'string|max:255',
            'notes' => 'string',
            'job_title' => 'string|max:255',
            'company' => 'string|max:255',
        ]);

        if ($validator->fails()) {
            throw CustomValidationException::fromValidator($validator);
        }
    }
}