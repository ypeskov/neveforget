<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky <xedelweiss@gmail.com>
 * Date: 08.8.16
 * Time: 21:42
 */

namespace App\Repositories\Person;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface PersonRepositoryInterface
{
    public function createPersonFromRequest(Request $request);

    public function updatePersonFromRequest($id, Request $request);

    public function deletePerson($id);

    public function getUserPersons(User $user);

    public function modelBelongsToUser(Model $model, $userId);

    public function getUserPerson($personId, $skipOwnershipCheck = false);

    public function all();
}