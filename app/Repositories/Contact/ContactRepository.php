<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/11/16
 * Time: 1:55 PM
 */

namespace App\Repositories\Contact;

use App\Errors\ErrorCode;
use App\Models\Contact;
use App\Models\Meta\Contact as Metacontact;
use App\Models\Person;
use App\Repositories\AbstractRepository;
use App\Repositories\Exception\NotFoundRepositoryException;
use App\Repositories\Exception\RepositoryException;
use App\Repositories\Person\PersonRepository;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ContactRepository extends AbstractRepository implements ContactRepositoryInterface
{
    protected $findable = [
        'contact_type',
        'social_source_record_id',
    ];
    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * ContactRepository constructor.
     * @param App $app
     * @param PersonRepository $personRepository
     */
    public function __construct(App $app, PersonRepository $personRepository)
    {
        parent::__construct($app);
        $this->personRepository = $personRepository;
    }

    public function model()
    {
        return Metacontact::class;
    }

    public function createContact($personId, $data)
    {
        $person = $this->personRepository->getById($personId);
        $data['person_id'] = $person->id;

        return $this->model->create($data);
    }

    public function createContactFromRequest($personId, Request $request)
    {
        $data = $this->extractDataFromRequest($request);
        return $this->createContact($personId, $data);
    }

    public function updateContact($personId, $contactId, $data)
    {
        /** @var Metacontact $model */
        $model = $this->getById($contactId);
        $this->failIfPersonDiffers($model, $personId);

        if (!$model->person->is_synced) {
            $model->update($data);
        } else {
            $overriddenModel = $model->overriddenContact;

            if (!$overriddenModel) {
                /** @var Metacontact $overriddenModel */
                $overriddenModel = $model->replicate();
                $overriddenModel->save();
                $model->overriddenContact()->associate($overriddenModel);
                $model->save();
            }

            $overriddenModel->update($data);
        }

        return $model;
    }

    public function updateContactFromRequest($personId, $contactId, Request $request)
    {
        $data = $this->extractDataFromRequest($request);

        return $this->updateContact($personId, $contactId, $data);
    }

    public function deleteContact($personId, $contactId)
    {
        /** @var Metacontact $model */
        $model = $this->getById($contactId);
        $this->failIfPersonDiffers($model, $personId);

        if (!$model->person->is_synced) {
            if (!$model->delete()) {
                throw new RepositoryException(ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
            }
        } else {
            $overriddenModel = $model->overriddenContact;

            if ($overriddenModel) {
                $overriddenModel->delete();
            } else {
                $model->is_hidden = true;
                $model->save();
            }
        }
    }

    public function getPersonContacts($personId)
    {
        return $this->model
            ->where('person_id', $personId)
            ->get();
    }

    public function getMetapersonContacts($metapersonId)
    {
        $persons = Person::where('parent_person_id', $metapersonId)
            ->orWhere('id', $metapersonId)
            ->get();

        return $this->model
            ->whereIn('person_id', $persons->pluck('id'))
            ->whereNull('overridden_contact_id')
            ->get();
    }

    /**
     * @param Model|Metacontact $model
     * @param $userId
     * @return bool
     */
    public function modelBelongsToUser(Model $model, $userId)
    {
        if ((int)$userId !== (int)$model->person->owner_id) { // @fixme is it ok to get Person without PersonRepository?
            return false;
        }

        return true;
    }

    public function getUserContact($personId, $contactId, $skipOwnershipCheck = false)
    {
        $model = $this->getById($contactId, $skipOwnershipCheck);
        $this->failIfPersonDiffers($model, $personId);

        return $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param Collection $collection
     * @param null $request
     * @return Collection
     * @throws \Exception
     */
    public function filterByRequest(Collection $collection, $request = null)
    {
        if (is_null($request)) {
            $request = \Request::all();
        }

        if ($request instanceof \Request) {
            $request = $request->all();
        }

        if (!is_array($request)) {
            throw new \Exception('Invalid request type');
        }

        $request = array_only($request, $this->findable);

        return $collection->filter(function (Metacontact $model) use ($request) {
            foreach ($request as $field => $value) {
                if ($model->{$field} != $value) {
                    return false;
                }
            }

            return true;
        })->values();
    }

    /**
     * @param Model|Metacontact $model
     * @param $personId
     * @throws NotFoundRepositoryException
     */
    protected function failIfPersonDiffers(Model $model, $personId)
    {
        if ((int)$model->person_id == (int)$personId) {
            return;
        }

        if ((int)$model->person->parent_person_id == (int)$personId) {
            return;
        }

        throw new NotFoundRepositoryException();
    }
}