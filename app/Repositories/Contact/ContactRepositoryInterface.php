<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 8/11/16
 * Time: 1:56 PM
 */

namespace App\Repositories\Contact;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface ContactRepositoryInterface
{
    public function createContactFromRequest($personId, Request $request);

    public function updateContactFromRequest($personId, $contactId, Request $request);

    public function deleteContact($personId, $contactId);

    public function getPersonContacts($personId);

    public function modelBelongsToUser(Model $model, $userId);

    public function getUserContact($personId, $contactId, $skipOwnershipCheck = false);

    public function all();

    public function filterByRequest(Collection $collection, $request = null);
}