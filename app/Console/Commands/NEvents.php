<?php
/**
 * This command manages in CLI neverforget events
 *
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 06.24.2016
 * Time: 05:54
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Event;
use App\User;

/**
 * Class NEvents means NeverForget Events
 *
 * @package App\Console\Commands
 */
class NEvents extends Command
{
    /**
     * This array stores the list of available commands
     *
     * @var array
     */
    private $availableCommands = [
        'add',
        'list',
    ];

    private $faker;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'events:manage {action=list}'
                            . ' {--today=2 : add new events dated by today date}'
                            . ' {--user=2 : user id to link events}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manages events, e.g. add/remove/update/etc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\Faker\Generator $faker)
    {
        parent::__construct();

        $this->faker = $faker;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');

        switch( $action ) {
            case    'add':
                $this->generateEvents($this->option('today'));
                break;
            case    'list':
                $this->printAvailableCommands();
                break;
            default:
                $this->error("Command not found. Please verify input");
                $this->printAvailableCommands();
                break;
        }
    }

    private function printAvailableCommands()
    {
        $this->info("Available commands:");
        foreach($this->availableCommands as $command) { $this->line("\t" . $command); }
    }

    private function generateEvents(int $qty)
    {
        $faker  = $this->faker;
        $user   = User::where('id', $this->option("user"))->firstOrFail();

        $eventDate = new \DateTime();

        for($i=0; $i < $qty; $i++) {
            $event = Event::create([
                'event_types_id'    => Event::TYPE_EMAIL,
                'event_name'        => $faker->sentence(2),
                'recipient'         => $user->email,
                'message_subject'   => $faker->sentence(4),
                'message_body'      => $faker->sentence(6),
                'message_signature' => $faker->name,
                'datetime_to_send'  => $eventDate,
                'is_active'         => 1,
                'comment'           => $faker->paragraph(3),
                'user_id'           => $user->id,
            ]);

            $this->info("Event create: id: {$event->id}, event name: {$event->event_name}");
        }
        $this->line("----------------------------------");
        $this->info("Totaly created: $i");
    }
}
