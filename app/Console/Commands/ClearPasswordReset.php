<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserResetPassword;
use Carbon\Carbon;

class ClearPasswordReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'password:clear-reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear table password reset for older than 15 minutes tokens';

    const MINUTES_TOKEN_ALIVE   = 15;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserResetPassword $userResetPassword)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        UserResetPassword
            ::where('created_at', '<', Carbon::now()->subMinutes(self::MINUTES_TOKEN_ALIVE))
            ->delete();
    }
}
