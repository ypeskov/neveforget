<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Event\EventRepository;
use App\Models\Event;
use Mail;
use App\Utils\Email\EmailWhiteListTrait;
use App\Utils\Email\EmailEnabledTrait;

class EmailsSend extends Command
{
    use EmailWhiteListTrait, EmailEnabledTrait;

    private $realSendCfg;
    private $whiteList;

    private $sentEmails = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send {--real-send : real sending, not emulate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command send/emulates notifications via email which are scheduled for today';

    private $eventRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EventRepository $eventRepo)
    {
        parent::__construct();

        $this->eventRepo    = $eventRepo;

        $this->realSendCfg  = config('real_send');
        $this->whiteList    = config('white_list');
    }

    /**
     * Execute the console command.
     *
     * @Illuminate\Config $config
     *
     * @return mixed
     */
    public function handle()
    {
        $now = time();
        $currentDate = gmdate("Y-m-d", $now);
        $currentHour = gmdate("H", $now);

        $todayEvents = $this->eventRepo->getEventsInCurrentHour($currentDate, $currentHour);

        // start processing of events only if it is enabled for all event types
        if ( $this->realSendCfg['is_enabled'] ) {
            $this->processEvents($todayEvents);
        }

    }

    /**
     * Go through each event and process it depending on the type of event and how to send.
     * Currently it just a stub to display events.
     *
     * @param $events \Traversable
     */
    protected function processEvents($events)
    {
        foreach($events as $event) {
            if ( $event->event_types_id === Event::TYPE_EMAIL ) {
                $this->filterEmailToSend($event);
            }
        }

        $this->line("---------------------------------------------");
        $this->info("Totally sent emails: {$this->sentEmails}");
    }

    /**
     * Start sending of emails.
     *
     * @param $event
     */
    private function filterEmailToSend($event)
    {
        $isEmailEnabled = $this->isEmailSendEnabled();
        $onlyWhiteList  = $this->isEmailAddressInWhiteList();

        if ( $isEmailEnabled ) {

            //check if we can send emails to anyone
            if ( !$onlyWhiteList ) {
                $this->dispatchEmail($event);
            } else { //otherwise send only to the white list
                if ( in_array($event->recipient, $this->whiteList['email'])) {
                    echo "{$event->recipient} is in WHITE LIST \t->\t SENT\n";
                    $this->dispatchEmail($event);

                    $this->sentEmails++;
                } else {
                    echo "{$event->recipient} is NOT in WHITE LIST \t->\t NOT SENT\n";
                }
            }
        }

    }

    /**
     * Initialize actual sending of email here.
     *
     * @param $event
     */
    private function dispatchEmail($event)
    {
        if ( !$this->option('real-send') ) {
            echo "Processing (WITHOUT SENDING) event id:{$event->id}\t {$event->event_name}\t  send to {$event->recipient}\n";

            return false;
        }

        echo "Processing event id:{$event->id}\t {$event->event_name}\t  send to {$event->recipient}\n";

        Mail::send('email_templates.test',
                [
                    'messageContent'    => $event->message_body,
                    'signature'         => $event->message_signature,
                ],
                function($message) use ($event) {
                    $message
                        ->to($event->recipient)
                        ->from(env('MAIL_FROM_ADDRESS'))
                        ->subject($event->message_subject);
        });
    }
}
