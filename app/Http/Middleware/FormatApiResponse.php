<?php

namespace App\Http\Middleware;

use App\Components\ApiFormatter\ApiFormatter;
use Closure;
use Illuminate\Http\JsonResponse;

class FormatApiResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (!$request->wantsJson()) {
            return $response;
        }

        if ($response instanceof JsonResponse) {
            return $response;
        }

        return (new ApiFormatter())->formatResponse($response);
    }
}
