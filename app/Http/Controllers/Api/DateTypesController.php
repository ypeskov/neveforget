<?php

namespace App\Http\Controllers\Api;

use App\Models\DateType;
use App\Http\Controllers\Controller;

class DateTypesController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     *
     * @todo repository
     */
    public function index()
    {
        return DateType::where('is_custom', false)
            ->orWhere('owner_id', \Auth::user()->id)
            ->get();
    }
}
