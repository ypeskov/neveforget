<?php

namespace App\Http\Controllers\Api;

use App\Components\SocialIntegration\SocialIntegration;
use App\Components\SocialIntegration\SocialState;
use App\Http\Controllers\Controller;
use App\Models\DateType;
use App\Models\Person;
use App\Models\Social;
use App\Models\SocialToken;
use App\User;
use ContactsGrabber\Contact;
use ContactsGrabber\Date;
use ContactsGrabber\Driver\Google;
use ContactsGrabber\Driver\Vkontakte;
use ContactsGrabber\Grabber;
use ContactsGrabber\Metaperson;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

// @todo move to repositories
class SocialsController extends Controller
{
    /*
     * Flow:
     *  client (user action)
     *  -> attach
     *  -> social auth page (user action)
     *  -> acceptCode
     *  -> client
     */

    public function attachStart(Request $request, $provider)
    {
        $socialState = SocialState::fromRequest($request);

        $socialAuthUrl = SocialIntegration::with($provider)
            ->setState($socialState->toString())
            ->getAuthorizationUrl();

        return redirect($socialAuthUrl);
    }

    public function acceptCode(Request $request, $provider)
    {
        $authorizationCode = $request->input('code');
        $socialState = SocialState::fromRequest($request);

        try {
            $accessToken = SocialIntegration::with($provider)
                ->setAuthorizationCode($authorizationCode)
                ->getAccessToken();

            $user = $this->getUserByJWT($socialState->getClientJWT());

            if ($user) {
                Social::createFromAccessToken($user, $provider, $accessToken);
                $result = $socialState->getClientReturnUrl();
            } else {
                $newSocialToken = SocialToken::create([
                    'provider' => $provider,
                    'access_token' => $accessToken,
                ]);
                $newUserInfo = SocialIntegration::with($provider)
                    ->setAccessToken($accessToken)
                    ->getUserInfo();

                // @todo плохо-плохо! move somewhere (login)
                if ($connectedSocial = Social::whereSocialId($newUserInfo['id'])->where('social_type', $provider)->first()) {
                    try {
                        foreach ($socialState->getSocialTokens() as $socialToken) {
                            Social::createFromSocialToken($connectedSocial->owner, $socialToken);
                        }
                    } catch (\Exception $e) {
                        \Log::error('Exception during social tokens attachment', [
                            'exception' => $e,
                            'tokens' => $request->input('social_tokens'),
                        ]);
                    }

                    $jwtToken = \JWTAuth::fromUser($connectedSocial->owner);
                    $result = $socialState->getClientReturnUrl() . '?jwt=' . $jwtToken;
                    return redirect($result);
                }

                $socialState->addSocialToken($newSocialToken);
                $socialState->addSuggestedUserInfo($newUserInfo);

                $result = $socialState->getClientReturnUrl(true);
            }
        } catch (\Exception $e) {
            \Log::error('Exception during authorization code acceptance', [
                'exception' => $e->getMessage(),
                'provider' => $provider,
                'file' => $e->getFile() . ':' . $e->getLine(),
                'trace' => $e->getTrace(),
            ]);
            $result = $socialState->getClientReturnUrl(true);
        }

        return redirect($result);
    }

    public function index()
    {
        return Social::whereOwnerId(\Auth::user()->id)->get();
    }

    public function detach($socialId)
    {
        Social::whereOwnerId(\Auth::user()->id)
            ->whereId($socialId)
            ->first()
            ->delete();
    }

    public function sync($socialId)
    {
        $social = Social::whereOwnerId(\Auth::user()->id)
            ->whereId($socialId)
            ->first();

        if (!$social) {
            return [];
        }

        if ($social->social_type == Social::TYPE_GOOGLE) {
            $driver = new Google(config('services.google.client_id'), config('services.google.client_secret'));
            $driver->setAccessToken($social->auth_info['access_token']);
        } elseif ($social->social_type == Social::TYPE_VKONTAKTE) {
            $driver = new Vkontakte(config('services.vkontakte.client_id'), config('services.vkontakte.client_secret'));
            $driver->setAccessToken($social->auth_info['access_token']['access_token']);
        } else {
            return [];
        }

        $contactsGrabber = new Grabber($driver);
        /** @var Metaperson[] $metapersons */
        $metapersons = $contactsGrabber->fetchContacts();

        $demoResponse = [];

        foreach ($metapersons as $metaperson) {
            /** @var Contact[] $contacts */
            $contacts = $metaperson->getContacts();
            $dates = $metaperson->getDates();

            if (count($dates) == 0 && count($contacts) == 0) {
                continue;
            }

            /** @var Person $person */
            $person = Person::updateOrCreate([
                'social_source_id' => $social->id,
                'social_source_record_id' => $metaperson->getSourceId(),
                'owner_id' => \Auth::user()->id,
            ], [
                'first_name' => $metaperson->getFirstName() ?: '',
                'last_name' => $metaperson->getLastName() ?: '',
                'nick_name' => $metaperson->getNickName() ?: '',
            ]);

            $demoResponse[] = $person;

            foreach ($contacts as $contact) {
                switch (true) {
                    case ($contact instanceof Contact\Phone):
                        $type = \App\Models\Contact::TYPE_PHONE;
                        $value = $contact->getPossibleCountry()
                            ? $contact->getFormattedValue($contact->getPossibleCountry())
                            : $contact->getValue();
                        break;
                    case ($contact instanceof Contact\Email):
                        $type = \App\Models\Contact::TYPE_EMAIL;
                        $value = $contact->getValue();
                        break;
                    case ($contact instanceof Contact\Vkontakte):
                        $type = \App\Models\Contact::TYPE_VKONTAKTE;
                        $value = $contact->getValue();
                        break;
                    default:
                        $type = null;
                        $value = null;
                }

                if (is_null($type) || is_null($value)) {
                    continue;
                }

                /** @var \App\Models\Contact $contactRecord */
                $contactRecord = \App\Models\Contact::updateOrCreate([
                    'contact_type' => $type,
                    'value' => $value,
                    'person_id' => $person->id,
                    // 'social_source_id' // @todo think how to sync, not reset
                ]);
            }

            foreach ($dates as $date) {
                $type = $date->getType();
                $value = $date->getDate();

                if (is_null($type) || is_null($value)) {
                    continue;
                }

                /** @var \App\Models\Contact $contactRecord */
                $dateRecord = \App\Models\Date::updateOrCreate([
                    'date_type_id' => DateType::updateOrCreate([
                        'title' => $type,
                        'is_custom' => true,
                        'owner_id' => \Auth::user()->id,
                    ])->id,
                    'date' => $value,
                    'person_id' => $person->id,
                    // 'social_source_id' // @todo think how to sync, not reset
                ]);
            }

        }

        return $demoResponse;

        return array_map(function (Metaperson $contact) {
            return [
                'name' => implode(' ', [$contact->getFirstName(), $contact->getLastName()]),
                'sourceId' => $contact->getSourceId(),
                'dates' => array_map(function (Date $date) {
                    return [
                        'type' => $date->getType(),
                        'date' => $date->getDate()->format('Y-m-d'),
                        'comment' => $date->getComment(),
                    ];
                }, $contact->getDates()),
                'contacts' => array_map(function (Contact $contact) {
                    return [
                        'type' => $contact->getType(),
                        'value' => $contact->getValue(),
                    ];
                }, $contact->getContacts()),
            ];
        }, $metapersons);
    }

    /**
     * @param $jwt
     * @return User|null
     */
    protected function getUserByJWT($jwt)
    {
        /** @var User $user */
        try {
            $user = \JWTAuth::setToken($jwt)->authenticate();
        } catch (\Exception $e) {
            $user = null;
        }

        return $user;
    }
}
