<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Exceptions\CustomValidationException;
use App\User;
use Auth;
use Hash;
use Lang;
use Validator;
use Tymon\JWTAuth\JWTAuth;
use Log;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, JWTAuth $jwtAuth)
    {
        $this->checkAccess($request->user['id']);

        $newUserData = $request->toArray()['user'];
        $user = User::find($id);
        if ( $user ) {
            $user->first_name   = $newUserData['first_name'];
            $user->last_name    = $newUserData['last_name'];
            $user->middle_name  = $newUserData['middle_name'];
            $user->email        = $newUserData['email'];
            $user->timezone     = $newUserData['timeZone'];

            $user->save();
        }

        $token = $jwtAuth->fromUser($user);

        return $token;
    }

    public function updatePassword(Request $request, $id)
    {
        $user = User::find($id);
        $password = $request->password;
        $newPassword = $request->newPassword;

        if ( !Hash::check($password, $user->password) ) {
            throw new CustomValidationException([
                'password' => Lang::get('Current password is incorrect'),
            ]);
        }

        $validator = $this->passwordValidator($request->toArray());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        $user
            ->fill([
                'password' => Hash::make($newPassword)
            ])
            ->save();

        return [];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Check if the resource for userId belongs to the current logged in user
     *
     * @param $userId
     * @return bool
     * @throws HttpException
     */
    protected function checkAccess($userId)
    {
        if ( (int) Auth::user()->id !== (int) $userId ) {
            throw new HttpException(BaseResponse::HTTP_FORBIDDEN);
        }
    }

    protected function passwordValidator(array $data)
    {
        return Validator::make($data, [
            'newPassword'      => 'required|min:6',
        ]);
    }
}
