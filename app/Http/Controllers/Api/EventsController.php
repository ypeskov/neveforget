<?php

namespace App\Http\Controllers\Api;

use App\Errors\ErrorCode;
use App\Models\Event;
use App\Repositories\Event\EventRepository;
use Auth;
use Log;
use Validator;
use Response;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\CustomValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Carbon\Carbon;

class EventsController extends Controller
{
    /**
     * EventsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index(EventRepository $repository, Request $request)
    {
        $eventsCollection = $repository->getUserEvents(\Auth::user());
        $events = $repository->filterByRequest($eventsCollection, $request);

        return $events;
    }

    /**
     * Creates a new event and stores to DB.
     *
     * @param EventRepository $repository
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(EventRepository $repository, Request $request)
    {
        $responseArr = $this->getDefaultResponseArr();
        
        $validator = $this->validateNewEvent($request);
        if ( $validator->fails() ) {
            throw CustomValidationException::fromValidator($validator);
        } else {
            try {
                $dateTimeInGMT = (new Carbon($request->get('datetime_to_send'), $request->get('timezone')))
                    ->setTimezone('GMT')
                    ->toDateTimeString();
                $request->merge(['datetime_to_send' => $dateTimeInGMT]);

                $newEvent = $repository->createEventFromRequest($request, Auth::user()->id)->toArray();
                $responseArr['event'] = $newEvent;
            } catch(\Exception $e) {
                throw new HttpException(BaseResponse::HTTP_INTERNAL_SERVER_ERROR,
                    ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
            }

        }

        return $responseArr;
    }

    public function update(EventRepository $repository, Request $request, $id)
    {
        /**
         * @var Event $event
         */
        $event = $repository->getUserEvent($id);

        $validator = $this->validateNewEvent($request);
        //check validation rules or that we don't change status value
        if ( $validator->fails() and $request->get("field", "") !== "status" ) {
            throw CustomValidationException::fromValidator($validator);
        } else {
            try {
                $dateTimeInGMT = (new Carbon($request->get('datetime_to_send'), $request->get('timezone')))
                    ->setTimezone('GMT')
                    ->toDateTimeString();
                $request->merge(['datetime_to_send' => $dateTimeInGMT]);

                $event->fill(array_filter($request->only($event->getFillable()), function($value) {
                    return !is_null($value);
                }));

                $event->save();

            } catch(\Exception $e) {
                throw new HttpException(BaseResponse::HTTP_INTERNAL_SERVER_ERROR,
                    ErrorCode::getErrorMsgByCode(ErrorCode::INTERNAL_ERROR));
            }
        }
        
        return $event;
    }
    
    public function show(EventRepository $repository, $id)
    {
        $event = $repository->getUserEvent($id);

        return $event;
    }

    public function delete(EventRepository $repository, $id)
    {
        $event = $repository->getUserEvent($id);

        $repository->deleteEvent($event->id);
    }

    /**
     * Validates request for new event creation.
     *
     * @param Request $request
     * @return Validator $validator
     */
    private function validateNewEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_name'        => 'required|max:' . Event::EVENT_NAME_MAX_LENGTH,
            'recipient'         => 'required|max:' . Event::RECIPIENT_MAX_LENGTH,
            'message_subject'   => 'required|max:' . Event::MESSAGE_SUBJECT_MAX_LENGTH,
            'message_body'      => 'required|max:65535',
            'message_signature' => 'required|max:' . Event::MESSAGE_SIGNATURE_MAX_LENGTH,
            'datetime_to_send'  => 'required:Y-m-d',
            'timezone'          => 'required',
        ]);

        return $validator;
    }
}
