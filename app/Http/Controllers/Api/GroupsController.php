<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CustomValidationException;
use App\Models\Group;
use App\Repositories\Exception\UnauthorizedRepositoryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

// @fixme do via repositories
class GroupsController extends Controller
{
    public function index(Request $request)
    {
        return Group::whereOwnerId($request->user()->id)
            ->get();
    }

    public function create(Request $request)
    {
        $title = $request->get('title');

        // @fixme validation
        if (!is_string($title) || Str::length($title) > 50) {
            throw new CustomValidationException([
                'title' => 'Invalid parameter value',
            ]);
        }

        $group = new Group();
        $group->title = $title;
        $group->owner_id = $request->user()->id;
        $group->save();

        return $group;
    }

    public function show(Request $request, $id)
    {
        /** @var Group $group */
        $group = Group::findOrFail($id);

        // @fixme auth check
        if ($group->owner_id != $request->user()->id) {
            throw new UnauthorizedRepositoryException();
        }

        return $group;
    }

    public function update(Request $request, $id)
    {
        $title = $request->get('title');

        /** @var Group $group */
        $group = Group::findOrFail($id);

        // @fixme auth check
        if ($group->owner_id != $request->user()->id) {
            throw new UnauthorizedRepositoryException();
        }

        // @fixme validation
        if (!is_string($title) || Str::length($title) > 50) {
            throw new CustomValidationException([
                'title' => 'Invalid parameter value',
            ]);
        }

        $group->title = $title;
        $group->save();

        return $group;
    }

    public function delete(Request $request, $id)
    {
        /** @var Group $group */
        $group = Group::findOrFail($id);

        // @fixme auth check
        if ($group->owner_id != $request->user()->id) {
            throw new UnauthorizedRepositoryException();
        }

        $group->delete();
    }
}
