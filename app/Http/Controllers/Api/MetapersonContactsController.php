<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Person;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Metaperson\MetapersonRepository;
use Illuminate\Http\Request;

class MetapersonContactsController extends Controller
{
    /**
     * @var Person
     */
    protected $metaperson;

    /**
     * MetapersonContactsController constructor.
     * @param MetapersonRepository $metapersonRepository
     */
    public function __construct(MetapersonRepository $metapersonRepository)
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index(ContactRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        $collection = $repository->getMetapersonContacts($metaperson->id);
        return $repository->filterByRequest($collection);
    }

    public function create(ContactRepository $repository, MetapersonRepository $metapersonRepository, Request $request, $metapersonId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->createContactFromRequest($metaperson->id, $request);
    }

    public function update(ContactRepository $repository, MetapersonRepository $metapersonRepository, Request $request, $metapersonId, $contactId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->updateContactFromRequest($metaperson->id, $contactId, $request);
    }

    public function show(ContactRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId, $contactId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->getUserContact($metaperson->id, $contactId);
    }

    public function delete(ContactRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId, $contactId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        $repository->deleteContact($metaperson->id, $contactId);
    }
}
