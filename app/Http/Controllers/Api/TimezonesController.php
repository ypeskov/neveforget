<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TimezonesController extends Controller
{
    public function index()
    {
        $timezones = \DateTimeZone::listIdentifiers (\DateTimeZone::ALL);

        return $timezones;
    }
}
