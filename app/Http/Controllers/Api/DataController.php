<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Auth;
use App\Repositories\Data\DataRepository;


class DataController extends Controller
{
    /**
     * Return common data like timezones, other dictionaries used by the app globally.
     *
     * @param Request $request
     * @return array
     */
    public function common(Request $request)
    {
        $data = [];

        $data['timeZones'] = \DateTimeZone::listIdentifiers (\DateTimeZone::ALL);

        return $data;
    }

    public function getUserContacts(DataRepository $dataRepository)
    {
        $contacts = $dataRepository->getUserContacts(Auth::user());

        return $contacts;
    }
}
