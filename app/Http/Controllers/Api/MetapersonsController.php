<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CustomValidationException;
use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Date\DateRepository;
use Illuminate\Http\Request;
use App\Repositories\Metaperson\MetapersonRepository;

class MetapersonsController extends Controller
{
    /**
     * MetapersonsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index(MetapersonRepository $repository)
    {
        $collection = $repository->getUserMetapersons(\Auth::user());

        return $repository->filterByRequest($collection);
    }

    public function create(MetapersonRepository $repository, Request $request)
    {
        $groups = $request->input('groups', []);

        $validator = \Validator::make($request->all(), [
            'groups.*.title' => 'required|string',
        ]);

        if ($validator->fails()) {
            // @fixme dirty hack to explode array
            $messages = $validator->errors()->getMessages();
            $result = [];
            foreach ($messages as $key => $value) {
                array_set($result, $key, $value);
            }
            throw new CustomValidationException($result);
        }

        \DB::beginTransaction();
        try {
            $person = $repository->createMetapersonFromRequest($request);
            $repository->setGroups($person->id, $groups);
            \DB::commit();

            return $person->load('groups');
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
    }

    public function update(MetapersonRepository $metapersonRepository, ContactRepository $contactRepository, DateRepository $dateRepository, Request $request, $id)
    {
        $metaperson = $request->input('metaperson', []);
        $contacts = $request->input('contacts', []);
        $dates = $request->input('dates', []);
        $groups = $request->input('metaperson.groups', []);

        $validator = \Validator::make($request->all(), [
            'metaperson.first_name' => 'string|max:255',
            'metaperson.last_name' => 'string|max:255',
            'metaperson.nick_name' => 'string|max:255',
            'metaperson.notes' => 'string',
            'metaperson.job_title' => 'string|max:255',
            'metaperson.company' => 'string|max:255',

            'contacts.*.id' => 'integer',
            'contacts.*.value' => 'required',

            'dates.*.id' => 'integer',
            'dates.*.date' => 'required',

            'metaperson.groups.*.title' => 'required|string',
        ]);

        if ($validator->fails()) {
            // @fixme dirty hack to explode array
            $messages = $validator->errors()->getMessages();
            $result = [];
            foreach ($messages as $key => $value) {
                array_set($result, $key, $value);
            }
            throw new CustomValidationException($result);
        }

        \DB::beginTransaction();
        try {
            $metapersonResult = $metapersonRepository->updateMetaperson($id, $metaperson);
            foreach ($contacts as &$contact) {
                if (isset($contact['id'])) {
                    $contact = $contactRepository->updateContact($id, $contact['id'], $contact);
                } else {
                    $contact = $contactRepository->createContact($id, $contact);
                }
            }
            foreach ($dates as &$date) {
                if (isset($date['id'])) {
                    $date = $dateRepository->updateDate($id, $date['id'], $date);
                } else {
                    $date = $dateRepository->createDate($id, $date);
                }
            }
            $metapersonRepository->setGroups($id, $groups);
            \DB::commit();

            return [
                'metaperson' => $metapersonResult->load('groups'),
                'contacts' => $contacts,
                'dates' => $dates,
            ];
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
    }

    public function show(MetapersonRepository $repository, $id)
    {
        return $repository->getUserMetaperson($id);
    }

    public function delete(MetapersonRepository $repository, $id)
    {
        $repository->deleteMetaperson($id);
    }

    public function merge(MetapersonRepository $repository, Request $request)
    {
        $persons = $request->get('metapersons');
        $metaperson = $repository->mergePersons($persons);

        return $metaperson;
    }

    public function persons(MetapersonRepository $repository, $id)
    {
        return $repository->getMetapersonPersons($id);
    }

    public function connect(MetapersonRepository $repository, $metapersonId, $personId)
    {
        return $repository->mergePerson($metapersonId, $personId);
    }

    public function disconnect(MetapersonRepository $repository, $metapersonId, $personId)
    {
        return $repository->unmergePerson($personId);
    }
}
