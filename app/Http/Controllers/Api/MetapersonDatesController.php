<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Person;
use App\Repositories\Date\DateRepository;
use App\Repositories\Metaperson\MetapersonRepository;
use Illuminate\Http\Request;

class MetapersonDatesController extends Controller
{
    /**
     * @var Person
     */
    protected $metaperson;

    /**
     * MetapersonDatesController constructor.
     * @param MetapersonRepository $metapersonRepository
     */
    public function __construct(MetapersonRepository $metapersonRepository)
    {
        parent::__construct();
    }

    public function index(DateRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        $collection = $repository->getMetapersonDates($metaperson->id);
        return $repository->filterByRequest($collection);
    }

    public function create(DateRepository $repository, MetapersonRepository $metapersonRepository, Request $request, $metapersonId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->createDateFromRequest($metaperson->id, $request);
    }

    public function update(DateRepository $repository, MetapersonRepository $metapersonRepository, Request $request, $metapersonId, $dateId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->updateDateFromRequest($metaperson->id, $dateId, $request);
    }

    public function show(DateRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId, $dateId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        return $repository->getUserDate($metaperson->id, $dateId);
    }

    public function delete(DateRepository $repository, MetapersonRepository $metapersonRepository, $metapersonId, $dateId)
    {
        $metaperson = $metapersonRepository->getUserMetaperson($metapersonId);
        $repository->deleteDate($metaperson->id, $dateId);
    }
}
