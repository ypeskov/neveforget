<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserResetPassword;
use App\User;
use App\Utils\Email\EmailEnabledTrait;
use App\Utils\Email\EmailWhiteListTrait;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Log;
use Mail;
use Validator;

/**
 * Class ForgotPasswordController
 * @package App\Http\Controllers\Api\Auth
 *
 * @todo Maybe this controller should be modernized using Laravel 5.4 features/methods
 */
class ForgotPasswordController extends Controller
{
    use EmailEnabledTrait;
    use EmailWhiteListTrait;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function sendResetLinkEmail(Request $request)
    {
        $resetEmail = $request->resetEmail;

        $validator = Validator::make($request->all(), [
            'resetEmail'    => 'required|email',
        ]);
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        try {
            $user = User::where('email', $resetEmail)->firstOrFail();

            $resetPassword = UserResetPassword::where('user_id', $user->id)->first();

            if ( ! (bool) $resetPassword ) {
                $token = UserResetPassword::getEmailToken($user->email);

                UserResetPassword::create([
                    'token'     => $token,
                    'user_id'   => $user->id,
                ]);
            } else {
                $token = $resetPassword->token;
            }

            $this->dispatchResetPasswordEmail($user, $token);
        } catch (ModelNotFoundException $e) {
            Log::warning("User with email [{$resetEmail}] not found.");
        } catch (\Exception $e ) {
            Log::error($e->getMessage());
        }

        return [
            'success'   => true,
        ];
    }

    protected function dispatchResetPasswordEmail($user, $token)
    {
        $isEmailEnabled = $this->isEmailSendEnabled();
        $onlyWhiteList  = $this->isEmailAddressInWhiteList();
        $whiteList      = config('white_list');

        if ( $isEmailEnabled) {
            //check if we can send emails to anyone
            if ( !$onlyWhiteList ) {
                $this->sendRestorePasswordEmail($user, $token);
            } else { //otherwise send only to the white list
                if ( in_array($user->email, $whiteList['email'])) {
                    Log::info("{$user->email} is in WHITE LIST \t->\t SENT");
                    $this->sendRestorePasswordEmail($user, $token);;
                } else {
                    Log::info("{$user->email} is NOT in WHITE LIST \t->\t NOT SENT");
                }
            }
        }

    }

    protected function sendRestorePasswordEmail($user, $token)
    {
        Mail::send('email_templates.reset_password',
            [
                'first_name'    => $user->first_name,
                'last_name'     => $user->last_name,
                'token'         => $token,
                'url'           => config('app.client_url'),
            ],
            function($message) use ($user) {
                $message
                    ->to($user->email)
                    ->from(env('MAIL_FROM_ADDRESS'))
                    ->subject(trans('auth.reset_password_link_email_subject'));
            });
    }

}
