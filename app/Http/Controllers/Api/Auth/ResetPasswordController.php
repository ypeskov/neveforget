<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserResetPassword;
use App\User;
use Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {
        $requestArr = $request->all();
        $validator = $this->passwordValidator($requestArr);
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        $resetPassword = UserResetPassword::where('token', $request->token)->firstOrFail();

        $user = User::find($resetPassword->user_id);
        $user
            ->fill([
                'password' => Hash::make($requestArr['password'])
            ])
            ->save();

        //and MUST delete token from DB
        $resetPassword->delete();

        return [
            'success'   => true,
        ];
    }

    protected function passwordValidator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|min:6',
        ]);
    }
}
