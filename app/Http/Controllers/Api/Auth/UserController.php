<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return User|null
     */
    public function user(Request $request)
    {
        return $request->user();
    }

    public function refreshToken(JWTAuth $jwtAuth)
    {
        return [
            'token' => $jwtAuth->parseToken()->refresh(),
        ];
    }

}
