<?php

namespace App\Http\Controllers\Api\Auth;

use App\Exceptions\CustomValidationException;
use App\Models\Social;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Tymon\JWTAuth\JWTGuard;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'max:255',
            'middle_name'   => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'timezone' => 'timezone',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        /** @var User $user */
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => array_get($data, 'last_name', ''),
            'middle_name' => array_get($data, 'middle_name', ''),
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'timezone' => array_get($data, 'timezone', config('app.timezone')),
        ]);

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        /** @var JWTGuard $guard */
        $guard = $this->guard();

        try {
            $this->attachSocialTokens($user, $request->input('social_tokens', []));
        } catch (\Exception $e) {
            \Log::error('Exception during social tokens attachment', [
                'exception' => $e,
                'tokens' => $request->input('social_tokens'),
            ]);
        }

        return [
            'token' => $guard->tokenById($user->id),
        ];
    }

    protected function attachSocialTokens($user, $tokens)
    {
        $validator = Validator::make($tokens, [
            '*.provider' => 'required|string',
            '*.token' => 'required|string',
        ]);

        if ($validator->fails()) {
            throw CustomValidationException::fromValidator($validator);
        }

        foreach ($tokens as $socialToken) {
            Social::createFromSocialToken($user, $socialToken);
        }
    }
}
