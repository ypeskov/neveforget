<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Errors\ErrorCode;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data;

    public function __construct()
    {
        // in case we'll need it
    }

    protected function getDefaultResponseArr()
    {
        return [
            'success'   => true,
            'code'      => ErrorCode::SUCCESS,
            'message'   => ErrorCode::getErrorMsgByCode(ErrorCode::SUCCESS),
            'data'      => [],
        ];
    }
}
