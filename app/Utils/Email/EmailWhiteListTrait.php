<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 29.11.16
 * Time: 22:10
 */

namespace App\Utils\Email;

trait EmailWhiteListTrait
{
    /**
     * @return bool
     */
    public function isEmailAddressInWhiteList()
    {
        return config('real_send')['email']['only_white_list'];
    }
}