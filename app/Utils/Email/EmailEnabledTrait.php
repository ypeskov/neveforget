<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 29.11.16
 * Time: 22:31
 */

namespace App\Utils\Email;

trait EmailEnabledTrait
{
    /**
     * @return bool
     */
    public function isEmailSendEnabled()
    {
        return config('real_send')['email']['is_enabled'];
    }
}