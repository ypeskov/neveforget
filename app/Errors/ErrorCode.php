<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 09.01.16
 * Time: 11:58
 */

namespace App\Errors;

use \Exception;

class ErrorCode
{
    const SUCCESS               = 100;

    const INTERNAL_ERROR        = 101;
    const UNKNOWN_ERROR_CODE    = 102;
    const INVALID_REQUEST_PARAM = 103;

    const UNAUTHORIZED          = 401;
    const ACCESS_DENIED         = 403;
    const NOT_FOUND             = 404;


    static private $errors = [
        self::SUCCESS               => 'success',

        self::INTERNAL_ERROR        => 'internal error',
        self::UNKNOWN_ERROR_CODE    => 'unknown error code',
        self::INVALID_REQUEST_PARAM => 'invalid request parameter',

        self::UNAUTHORIZED          => 'unauthorized',
        self::ACCESS_DENIED         => 'access denied',
        self::NOT_FOUND             => 'not found',
    ];

    static public function getErrorMsgByCode($code)
    {
        if ( isset(self::$errors[$code]) ) {
            return self::$errors[$code];
        } else {
            throw new Exception(ErrorCode::getErrorMsgByCode(self::UNKNOWN_ERROR_CODE));
        }
    }
}