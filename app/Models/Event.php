<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property int $event_types_id
 * @property int $user_id
 * @property string $event_name
 * @property string $recipient
 * @property string $message_subject
 * @property string $message_body
 * @property string $message_signature
 * @property \Carbon\Carbon $datetime_to_send
 * @property string $timezone
 * @property string $comment
 * @property bool $is_active
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereDatetimeToSend($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereEventName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereEventTypesId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereMessageBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereMessageSignature($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereMessageSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereRecipient($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereTimezone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event whereUserId($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use SoftDeletes;

    const EVENT_NAME_MAX_LENGTH         = 100;
    const RECIPIENT_MAX_LENGTH          = 150;
    const MESSAGE_SUBJECT_MAX_LENGTH    = 200;
    const MESSAGE_SIGNATURE_MAX_LENGTH  = 50;
    const COMMENT_LENGTH                = 500;
    const TABLE_NAME    = 'events';

    const TYPE_EMAIL    = 1;
    const TYPE_SMS      = 2;
    const TYPE_IRC      = 3;
    const TYPE_FACEBOOK = 4;
    const TYPE_TWITTER  = 5;
    
    protected $table = self::TABLE_NAME;

    protected $dates = [
        'datetime_to_send',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name',
        'event_types_id',
        'recipient',
        'message_subject',
        'message_body',
        'comment',
        'message_signature',
        'datetime_to_send',
        'timezone',
        'user_id',
        'is_active',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
