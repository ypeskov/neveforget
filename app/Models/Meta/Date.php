<?php

namespace App\Models\Meta;

/**
 * App\Models\Meta\Date
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $person_id
 * @property int $date_type_id
 * @property \Carbon\Carbon $date
 * @property int $overridden_date_id
 * @property-read \App\Models\DateType $dateType
 * @property-read mixed $contact_type
 * @property-read \App\Models\Date $originalDate
 * @property-read \App\Models\Date $overriddenDate
 * @property-read \App\Models\Person $person
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereDateTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereOverriddenDateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date wherePersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Date whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Date extends \App\Models\Date
{
    protected $with = [];

    protected $visible = [
        'id',
        'date',
        'date_type_id',
        'person_id',
        'dateType',

        'person',
    ];

    public function getDateAttribute()
    {
        $result = $this->overriddenDate
            ? $this->overriddenDate->date
            : $this->attributes['date'];

        return $this->serializeDate(
            $this->asDateTime($result)
        );
    }

    public function getContactTypeAttribute()
    {
        return $this->overriddenDate
            ? $this->overriddenDate->date_type_id
            : $this->attributes['date_type_id'];
    }
}
