<?php

namespace App\Models\Meta;

/**
 * App\Models\Meta\Contact
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $person_id
 * @property string $value
 * @property bool $is_hidden
 * @property string $social_source_record_id
 * @property string $contact_type
 * @property int $overridden_contact_id
 * @property-read \App\Models\Contact $originalContact
 * @property-read \App\Models\Contact $overriddenContact
 * @property-read \App\Models\Person $person
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereContactType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereIsHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereOverriddenContactId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact wherePersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereSocialSourceRecordId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Meta\Contact whereValue($value)
 * @mixin \Eloquent
 */
class Contact extends \App\Models\Contact
{
    protected $with = [];

    protected $visible = [
        'id',
        'contact_type',
        'value',
        'person_id',

        'person',
    ];

    public function getValueAttribute()
    {
        return $this->overriddenContact
            ? $this->overriddenContact->value
            : $this->attributes['value'];
    }

    public function getContactTypeAttribute()
    {
        return $this->overriddenContact
            ? $this->overriddenContact->contact_type
            : $this->attributes['contact_type'];
    }
}
