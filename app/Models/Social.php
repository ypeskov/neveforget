<?php

namespace App\Models;

use App\Components\SocialIntegration\SocialIntegration;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Social
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $social_type
 * @property string $social_id
 * @property array $info
 * @property array $auth_info
 * @property int $owner_id
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $persons
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereAuthInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereSocialId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereSocialType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Social whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Social extends Model
{
    use SoftDeletes;

    const TYPE_VKONTAKTE = 'vkontakte';
    const TYPE_FACEBOOK = 'facebook';
    const TYPE_GOOGLE = 'google';

    /**
     * These fields should be used only by backend.
     *
     * @var array
     */
    protected $hidden = [
        'auth_info',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'info' => 'array',
        'auth_info' => 'array',
    ];

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_VKONTAKTE,
            self::TYPE_FACEBOOK,
            self::TYPE_GOOGLE
        ];
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function persons()
    {
        return $this->hasMany(Person::class, 'social_source_id');
    }

    /**
     * @param User $user
     * @param SocialToken $socialToken
     * @return static
     * @throws \Exception
     * @todo Move to repository
     */
    public static function createFromSocialToken($user, $socialToken)
    {
        if (is_array($socialToken)) {
            /** @var SocialToken $socialToken */
            $socialToken = SocialToken::whereToken($socialToken['token'])
                ->where('is_active', true)
                ->first();
        }

        if (!$socialToken) {
            throw new \Exception('Invalid social token: ' . json_encode($socialToken));
        }

        $result = static::createFromAccessToken($user, $socialToken->provider, $socialToken->access_token);

        // disable SocialToken
        $socialToken->is_active = false;
        $socialToken->save();

        return $result;
    }

    /**
     * @param User $user
     * @param $provider
     * @param $accessToken
     * @return static
     * @throws \Exception
     *
     * @todo replace general exception with specific one
     */
    public static function createFromAccessToken($user, $provider, $accessToken)
    {
        $userInfo = SocialIntegration::with($provider)
            ->setAccessToken($accessToken)
            ->getUserInfo();

        if (Social::whereSocialId($userInfo['id'])->where('social_type', $provider)->first()) {
            throw new \Exception('Social is already connected');
        }

        $result = new static();
        $result->social_type = $provider;
        $result->social_id = $userInfo['id'];
        $result->info = $userInfo;
        $result->auth_info = [
            'access_token' => $accessToken,
        ];
        $result->owner_id = $user->id;
        $result->save();

        return $result;
    }
}
