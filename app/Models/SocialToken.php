<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialToken
 *
 * @property int $id
 * @property string $token
 * @property bool $is_active
 * @property string $provider
 * @property array $access_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereAccessToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereProvider($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialToken whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SocialToken extends Model
{
    protected $table = 'social_tokens';
    protected $fillable = [
        'provider', 'access_token',
    ];

    protected $visible = [
        'provider', 'token',
    ];

    protected $casts = [
        'access_token' => 'array',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setAttribute('token', $this->generateToken());
    }

    protected function generateToken()
    {
        return bin2hex(random_bytes(15));
    }

}
