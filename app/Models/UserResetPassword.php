<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserResetPassword
 *
 * @property int $id
 * @property string $token
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResetPassword whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResetPassword whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResetPassword whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResetPassword whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResetPassword whereUserId($value)
 * @mixin \Eloquent
 */
class UserResetPassword extends Model
{
    protected $fillable = [
        'token',
        'user_id',
    ];

    static public function getEmailToken($email)
    {
        return password_hash($email, PASSWORD_DEFAULT);
    }
}
