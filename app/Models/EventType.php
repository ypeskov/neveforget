<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\EventType
 *
 * @property int $id
 * @property string $event_type_name
 * @property string $event_type_code
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereEventTypeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereEventTypeName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EventType extends Model
{
    use SoftDeletes;

    const TABLE_NAME    = 'event_types';
    const EVENT_TYPE_CODE_MAX_LENGTH    = 20;
    const EVENT_TYPE_NAME_MAX_LENGTH    = 50;

    protected $table = self::TABLE_NAME;
}
