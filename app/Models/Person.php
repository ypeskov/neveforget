<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Person
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $first_name
 * @property string $last_name
 * @property string $nick_name
 * @property string $notes
 * @property int $owner_id
 * @property string $job_title
 * @property string $company
 * @property int $social_source_id
 * @property string $social_source_record_id
 * @property \Carbon\Carbon $last_synced_at
 * @property int $parent_person_id
 * @property bool $is_hidden
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Date[] $dates
 * @property-read mixed $is_synced
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $mergedPersons
 * @property-read \App\User $owner
 * @property-read \App\Models\Person $parentPerson
 * @property-read \App\Models\Social $syncSource
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereIsHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereJobTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereLastSyncedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereNickName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereParentPersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereSocialSourceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereSocialSourceRecordId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Person whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Person extends Model
{
    use SoftDeletes;

    protected $table = 'persons';

    protected $with = ['syncSource', 'groups'];

    protected $appends = ['is_synced'];

    protected $dates = ['last_synced_at']; // @todo conflict with dates()?

    protected $fillable = [
        'owner_id',
        'first_name',
        'last_name',
        'nick_name',
        'notes',
        'job_title',
        'company',
        'social_source_id',
        'social_source_record_id',
        'is_hidden',
    ];

    protected $casts = [
        'is_hidden' => 'boolean',
    ];

    protected $attributes = [
        'notes' => '',
        'job_title' => '',
        'company' => '',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function dates()
    {
        return $this->hasMany(Date::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class)->whereNull('overridden_contact_id');
    }

    public function syncSource()
    {
        return $this->belongsTo(Social::class, 'social_source_id');
    }

    /*
     * Metaperson methods
     * @fixme move them to separated class?
     */

    public function mergedPersons()
    {
        return $this->hasMany(Person::class, 'parent_person_id');
    }

    /**
     * Should be local (social_source_id == null)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentPerson()
    {
        return $this->belongsTo(Person::class, 'parent_person_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'groups_persons');
    }

    public function getIsSyncedAttribute()
    {
        return $this->social_source_id != null;
    }
}
