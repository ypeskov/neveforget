<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DateType
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $title
 * @property array $recurring_rules
 * @property bool $is_custom
 * @property int $owner_id
 * @property-read \App\User $owner
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereIsCustom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereRecurringRules($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DateType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DateType extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'recurring_rules' => 'array',
        'is_custom' => 'boolean',
    ];

    protected $visible = [
        'id',
        'title',
        'is_custom',
        'owner_id',
    ];

    protected $fillable = [
        'title', 'is_custom', 'owner_id', 'recurring_rules',
    ];

    protected $attributes = [
        'recurring_rules' => '{}',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }
}
