<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Date
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $person_id
 * @property int $date_type_id
 * @property \Carbon\Carbon $date
 * @property int $overridden_date_id
 * @property-read \App\Models\DateType $dateType
 * @property-read \App\Models\Date $originalDate
 * @property-read \App\Models\Date $overriddenDate
 * @property-read \App\Models\Person $person
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereDateTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereOverriddenDateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date wherePersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Date whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Date extends Model
{
    use SoftDeletes;

    protected $dates = ['date'];

    protected $with = ['dateType'];

    protected $fillable = [
        'person_id',
        'date',
        'date_type_id',
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function dateType()
    {
        return $this->belongsTo(DateType::class);
    }

    public function overriddenDate()
    {
        return $this->belongsTo(Date::class, 'overridden_date_id');
    }

    public function originalDate()
    {
        return $this->hasOne(Date::class, 'overridden_date_id');
    }
}
