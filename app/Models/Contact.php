<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Contact
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $person_id
 * @property string $value
 * @property bool $is_hidden
 * @property string $social_source_record_id
 * @property string $contact_type
 * @property int $overridden_contact_id
 * @property-read \App\Models\Contact $originalContact
 * @property-read \App\Models\Contact $overriddenContact
 * @property-read \App\Models\Person $person
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereContactType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereIsHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereOverriddenContactId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact wherePersonId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereSocialSourceRecordId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Contact whereValue($value)
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use SoftDeletes;

    const TYPE_PHONE = 'phone';
    const TYPE_EMAIL = 'email';
    const TYPE_VKONTAKTE = 'vkontakte';
    const TYPE_FACEBOOK = 'facebook';
    const TYPE_SKYPE = 'skype';
    const TYPE_VIBER = 'viber';
    const TYPE_TELEGRAM = 'telegram';
    const TYPE_WHATSAPP = 'whatsapp';

    protected $dates = ['last_synced_at'];

    protected $with = ['overriddenContact'];

    protected $fillable = [
        'contact_type',
        'value',
        'person_id',
        'is_hidden',
        'overridden_contact_id',
    ];

    protected $casts = [
        'is_hidden' => 'boolean',
    ];

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_PHONE,
            self::TYPE_EMAIL,
            self::TYPE_VKONTAKTE,
            self::TYPE_FACEBOOK,
            self::TYPE_SKYPE,
            self::TYPE_VIBER,
            self::TYPE_TELEGRAM,
            self::TYPE_WHATSAPP,
        ];
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function overriddenContact()
    {
        return $this->belongsTo(Contact::class, 'overridden_contact_id');
    }

    public function originalContact()
    {
        return $this->hasOne(Contact::class, 'overridden_contact_id');
    }
}
