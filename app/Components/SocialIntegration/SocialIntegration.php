<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 21/12/16
 * Time: 18:06
 */

namespace App\Components\SocialIntegration;

use GuzzleHttp\Client;
use Socialite;

class SocialIntegration
{
    protected $provider;
    protected $state = [];
    protected $redirectUrl;
    protected $scopes = [];

    protected $authorizationCode;
    protected $accessToken;

    /**
     * SocialIntegration constructor.
     * @param string $provider
     */
    public function __construct(string $provider)
    {
        $this->setProvider($provider);
        $this->setRedirectUrl(route('social.accept-code', ['provider' => $provider]));
        $this->setScopes(config("services.{$provider}.scopes", []));
    }

    /**
     * @param string $provider Supported: vkontakte, facebook, google
     * @return static
     */
    public static function with($provider)
    {
        return new static($provider);
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl()
    {
        return Socialite::with($this->getProvider())
                ->scopes($this->getScopes())
                ->stateless()
                ->redirectUrl($this->getRedirectUrl())
                ->redirect()
                ->getTargetUrl() . '&state=' . urlencode($this->getState());
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     * @return SocialIntegration
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return SocialIntegration
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param mixed $redirectUrl
     * @return SocialIntegration
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param mixed $scopes
     * @return SocialIntegration
     */
    public function setScopes($scopes)
    {
        $this->scopes = $scopes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * @param mixed $authorizationCode
     * @return SocialIntegration
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;

        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getAccessToken()
    {
        if (!$this->accessToken) {
            $this->setAccessTokenFromAuthorizationCode($this->getAuthorizationCode());
        }

        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     * @return SocialIntegration
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return array
     */
    public function getUserInfo()
    {
        switch ($this->getProvider()) {
            case 'vkontakte':
                $response = json_decode($this->request("https://api.vk.com/method/users.get?access_token={$this->getAccessToken()['access_token']}&https=1"), true);
                $userInfo = [
                    'id' => array_get($response, 'response.0.uid'),
                    'first_name' => array_get($response, 'response.0.first_name'),
                    'last_name' => array_get($response, 'response.0.last_name'),
                ];
                break;
            case 'facebook':
                $response = json_decode($this->request("https://graph.facebook.com/me?fields=id,first_name,last_name,email&access_token={$this->getAccessToken()['access_token']}"), true);
                $userInfo = [
                    'id' => array_get($response, 'id'),
                    'first_name' => array_get($response, 'first_name'),
                    'last_name' => array_get($response, 'last_name'),
                    'email' => array_get($response, 'email'),
                ];
                break;
            case 'google':
                $response = $this->request('https://www.googleapis.com/plus/v1/people/me?', [
                    'query' => [
                        'prettyPrint' => 'false',
                    ],
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer '.$this->getAccessToken()['access_token'],
                    ],
                ]);
                $response = json_decode($response, true);

                $email = '';
                foreach (array_get($response, 'emails') as $emailContainer) {
                    if ($emailContainer['type'] == 'account') {
                        $email = $emailContainer['value'];
                        break;
                    }
                }

                $userInfo = [
                    'id' => array_get($response, 'id'),
                    'first_name' => array_get($response, 'name.givenName'),
                    'last_name' => array_get($response, 'name.familyName'),
                    'email' => $email,
                ];
                break;
            default:
                $userInfo = [];
        }

        // stupid Socialite with its protected(!) getUserByToken method
        return $userInfo;
    }

    /**
     * @param $authorizationCode
     * @return SocialIntegration
     * @throws \Exception
     */
    protected function setAccessTokenFromAuthorizationCode($authorizationCode)
    {
        if (!$authorizationCode) {
            throw new \Exception('Empty authorization code passed');
        }

        $accessTokenResponse = Socialite::with($this->getProvider())
            ->redirectUrl($this->getRedirectUrl())
            ->getAccessTokenResponse($authorizationCode);

        $accessToken = $accessTokenResponse; //array_get($accessTokenResponse, 'access_token');
        $this->setAccessToken($accessToken);

        return $this;
    }

    private function request($url, $options = [])
    {
        try {
            $client = new Client();
            $response = $client->get($url, $options);

            return $response->getBody()->getContents();
        } catch (\Exception $exception) {
            \Log::error('SocialIntegration request exception', [
                'url' => $url,
                'options' => $options,
                'exception' => $exception,
            ]);
        }
    }
}