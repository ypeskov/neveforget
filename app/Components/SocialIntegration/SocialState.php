<?php
/**
 * Created by PhpStorm.
 * User: Michael Sverdlikovsky
 * Date: 21/12/16
 * Time: 18:51
 */

namespace App\Components\SocialIntegration;

use App\Models\SocialToken;
use Illuminate\Http\Request;

class SocialState
{
    const CLIENT_RETURN_URL_KEY = 'returnUrl';
    const CLIENT_JWT_KEY = 'jwt';
    const SOCIAL_TOKENS_KEY = 'tokens';
    const SUGGESTED_USER_INFO_KEY = 'userInfo';

    protected $socialTokens = [];
    protected $clientReturnUrl;
    protected $clientJWT;
    protected $suggestedUserInfo = [];

    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request)
    {
        $result = new static();

        if ($state = $request->input('state')) {
            // redirect from authorization url
            $state = json_decode(base64_decode($state), true);

            $clientReturnUrl = array_get($state, self::CLIENT_RETURN_URL_KEY);
            $clientJWT = array_get($state, self::CLIENT_JWT_KEY);
            $socialTokens = array_get($state, self::SOCIAL_TOKENS_KEY, []);
            $suggestedUserInfo = array_get($state, self::SUGGESTED_USER_INFO_KEY, []);
        } else {
            // request from client

            $clientReturnUrl = $request->input(self::CLIENT_RETURN_URL_KEY);
            $clientJWT = $request->input(self::CLIENT_JWT_KEY);

            // one GET param is sent to/from client
            $socials = $request->input('socials');
            $socials = json_decode(base64_decode($socials), true);

            $socialTokens = array_get($socials, self::SOCIAL_TOKENS_KEY, []);
            $suggestedUserInfo = array_get($socials, self::SUGGESTED_USER_INFO_KEY, []);
        }

        $result->setClientReturnUrl($clientReturnUrl);
        $result->setClientJWT($clientJWT);
        $result->setSocialTokens($socialTokens);
        $result->setSuggestedUserInfo($suggestedUserInfo);

        return $result;
    }

    public function toString($isForAuthProvider = true)
    {
        $result = [
            self::SOCIAL_TOKENS_KEY => $this->getSocialTokens(),
            self::SUGGESTED_USER_INFO_KEY => $this->getSuggestedUserInfo(),
        ];

        if ($isForAuthProvider) {
            $result = array_merge($result, [
                self::CLIENT_RETURN_URL_KEY => $this->getClientReturnUrl(),
                self::CLIENT_JWT_KEY => $this->getClientJWT(),
            ]);
        }

        return base64_encode(json_encode($result));
    }

    /**
     * @return array
     */
    public function getSocialTokens(): array
    {
        return $this->socialTokens;
    }

    /**
     * @param array|SocialToken $socialToken
     * @return SocialState
     */
    public function addSocialToken($socialToken)
    {
        $this->socialTokens[] = $socialToken instanceof SocialToken ? $socialToken->toArray() : $socialToken;
        return $this;
    }

    /**
     * @param array $socialTokens
     * @return SocialState
     */
    public function setSocialTokens(array $socialTokens): SocialState
    {
        $this->socialTokens = $socialTokens;
        return $this;
    }

    /**
     * @param bool $withState
     * @return mixed
     */
    public function getClientReturnUrl($withState = false)
    {
        $result = $this->clientReturnUrl;

        if ($withState) {
            $joiner = strpos($result, '?') === false ? '?' : '&';
            $result .= $joiner . 'socials=' . urlencode($this->toString(false));
        }

        return $result;
    }

    /**
     * @param mixed $clientReturnUrl
     * @return SocialState
     */
    public function setClientReturnUrl($clientReturnUrl)
    {
        $this->clientReturnUrl = $clientReturnUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientJWT()
    {
        return $this->clientJWT;
    }

    /**
     * @param mixed $clientJWT
     * @return SocialState
     */
    public function setClientJWT($clientJWT)
    {
        $this->clientJWT = $clientJWT;
        return $this;
    }

    /**
     * @return array
     */
    public function getSuggestedUserInfo(): array
    {
        return $this->suggestedUserInfo;
    }

    /**
     * @param array $suggestedUserInfo
     * @return SocialState
     */
    public function setSuggestedUserInfo(array $suggestedUserInfo): SocialState
    {
        $this->suggestedUserInfo = $suggestedUserInfo;
        return $this;
    }

    /**
     * @param array $suggestedUserInfo
     * @return SocialState
     */
    public function addSuggestedUserInfo(array $suggestedUserInfo): SocialState
    {
        $this->suggestedUserInfo = array_merge($this->getSuggestedUserInfo(), array_filter($suggestedUserInfo));
        return $this;
    }

}