<?php

namespace App\Components\ApiFormatter\Adapters;

abstract class AbstractAdapter
{
    const TYPE_REDIRECT = 'redirect';
    const TYPE_STRING = 'string';
    const TYPE_COLLECTION = 'collection';
    const TYPE_MIXED = 'mixed';
    const TYPE_VIEW = 'view';
    const TYPE_NULL = 'null';

    abstract public function format($data);

    /**
     * @return bool
     */
    protected function isDebugEnabled()
    {
        return config('app.debug');
    }
}