<?php

namespace App\Components\ApiFormatter\Adapters;

use App\Errors\ErrorCode;
use App\Repositories\Exception\NotFoundRepositoryException;
use App\Repositories\Exception\UnauthorizedRepositoryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionAdapter extends AbstractAdapter
{
    /**
     * @param \Exception $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function format($exception)
    {
        // Define the response
        $result = [
            'errors' => trans('messages.sorry'),
        ];

        // Default response of 400
        $statusCode = 400;
        $addDebugData = $this->isDebugEnabled();

        $result['type'] = self::TYPE_COLLECTION;

        switch (true) {
            case $exception instanceof HttpException:
                $statusCode = $exception->getStatusCode();
                $result['errors'] = [$exception->getMessage()];
                break;
            case $exception instanceof ValidationException:
                $result['errors'] = $exception->validator->errors();
                $addDebugData = false;
                break;
            case $exception instanceof ModelNotFoundException:
            case $exception instanceof NotFoundRepositoryException:
                $statusCode = ErrorCode::NOT_FOUND;
                $result['errors'] = [ErrorCode::getErrorMsgByCode($statusCode)];
                break;
            case $exception instanceof AuthenticationException:
                $statusCode = ErrorCode::UNAUTHORIZED;
                $result['errors'] = [ErrorCode::getErrorMsgByCode($statusCode)];
                break;
            case $exception instanceof UnauthorizedRepositoryException:
            case $exception instanceof UnauthorizedException:
                $statusCode = ErrorCode::ACCESS_DENIED;
                $result['errors'] = [ErrorCode::getErrorMsgByCode($statusCode)];
                break;
        }

        // Prepare response
        $response = [
            'success' => false,
            'result' => $result,
            'meta' => [
                'version' => config('app.version.api'),
                'request' => \Request::method() . ' ' . \Request::url(),
            ],
        ];

        // If the app is in debug mode && not Validation exception
        if ($addDebugData)
        {
            $response['debug'] = [
                'exception' => get_class($exception),
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTrace(),
            ];
        }

        // Return a JSON response with the response array and status code
        return response()->json($response, $statusCode);
    }
}