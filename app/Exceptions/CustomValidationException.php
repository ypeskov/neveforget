<?php

namespace App\Exceptions;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;

class CustomValidationException extends ValidationException
{
    protected $messages;

    /**
     * Create a new validation exception instance.
     *
     * @param array $messages
     * @param null $response
     */
    public function __construct(array $messages, $response = null)
    {
        $this->messages = new MessageBag($messages);
        $this->response = $response;

        // to be compatible with ValidationException interface
        $this->validator = new class ($messages) {
            private $messages;

            public function __construct($messages)
            {
                $this->messages = $messages;
            }

            public function errors()
            {
                return $this->messages;
            }
        };
    }

    public static function fromValidator(Validator $validator)
    {
        return new static($validator->errors()->getMessages()); // @todo use error codes
    }

    public function errors()
    {
        return $this->messages;
    }

}