<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// socials
Route::get('/socials/{provider}/attach', 'Api\SocialsController@attachStart')->name('social.attach-start');
Route::get('/socials/{provider}/accept-code', 'Api\SocialsController@acceptCode')->name('social.accept-code');

// auth
Route::post('/auth/login', 'Api\Auth\LoginController@login')->name('api.auth.login');
Route::post('/auth/register', 'Api\Auth\RegisterController@register')->name('api.auth.register');

Route::post('/password/reset-link', 'Api\Auth\ForgotPasswordController@sendResetLinkEmail')->name('api.password.reset-link');
Route::put('/password/set-new-password', 'Api\Auth\ResetPasswordController@reset')->name('api.password.set-new-password');

Route::group(['middleware' => ['auth:api', 'jwt.auth']], function(){

    Route::group(['prefix' => '/auth'], function() {
        Route::post('/logout', 'Api\Auth\LoginController@logout')->name('api.auth.logout');
        Route::get('/user', 'Api\Auth\UserController@user')->name('api.auth.user');
        Route::post('/refresh-token', 'Api\Auth\UserController@refreshToken')->name('api.auth.refresh-token');
    });

    // events
    Route::get('/events', 'Api\EventsController@index')->name('api.events.index');
    Route::post('/events', 'Api\EventsController@create')->name('api.events.create');
    Route::get('/events/{id}', 'Api\EventsController@show')->name('api.events.show');
    Route::put('/events/{id}', 'Api\EventsController@update')->name('api.events.update');
    Route::delete('/events/{id}', 'Api\EventsController@delete')->name('api.events.delete');

    Route::get('/timezones', 'Api\TimezonesController@index')->name('api.timezones.index');

    //users endpoints
    Route::group(['prefix' => '/users'], function() {
        Route::put('/{id}', 'Api\UsersController@update')->name('api.users.update');
        Route::put('/{id}/password', 'Api\UsersController@updatePassword')
            ->name('api.users.updatePassword');
    });

    //data endpoints
    Route::group(['prefix' => '/data'], function() {
        Route::get('/common', 'Api\DataController@common')->name('api.data.common');
        Route::get('/date-types', 'Api\DateTypesController@index')->name('api.data.date-types');
        Route::get('/user-contacts', 'Api\DataController@getUserContacts')->name('api.data.user-contacts');
    });

    Route::group(['prefix' => '/socials'], function() {
        Route::get('/', 'Api\SocialsController@index')->name('api.socials.index');
        Route::post('/{id}/sync', 'Api\SocialsController@sync')->name('api.socials.sync');
        Route::delete('/{id}', 'Api\SocialsController@detach')->name('api.socials.detach');
    });

    Route::group(['prefix' => '/groups'], function () {
        Route::get('/', 'Api\GroupsController@index')->name('api.groups.index');
        Route::post('/', 'Api\GroupsController@create')->name('api.groups.create');
        Route::get('/{id}', 'Api\GroupsController@show')->name('api.groups.show');
        Route::put('/{id}', 'Api\GroupsController@update')->name('api.groups.update');
        Route::delete('/{id}', 'Api\GroupsController@delete')->name('api.groups.delete');
    });

    // metapersons (virtual entity which unites multiple persons)
    Route::group(['prefix' => '/metapersons'], function(){
        Route::get('/', 'Api\MetapersonsController@index')->name('api.metapersons.index');
        Route::post('/', 'Api\MetapersonsController@create')->name('api.metapersons.create');
        Route::get('/{id}', 'Api\MetapersonsController@show')->name('api.metapersons.show');
        Route::put('/{id}', 'Api\MetapersonsController@update')->name('api.metapersons.update');
        Route::delete('/{id}', 'Api\MetapersonsController@delete')->name('api.metapersons.delete');

        Route::post('/merge', 'Api\MetapersonsController@merge')->name('api.metapersons.merge');

        Route::group(['prefix' => '{metaperson}/persons'], function () {
            Route::get('/', 'Api\MetapersonsController@persons')->name('api.metaperson.persons.index');
            Route::put('/{id}', 'Api\MetapersonsController@connect')->name('api.metaperson.persons.connect');
            Route::delete('/{id}', 'Api\MetapersonsController@disconnect')->name('api.metaperson.persons.disconnect');
        });

        Route::group(['prefix' => '{metaperson}/contacts'], function () {
            Route::get('/', 'Api\MetapersonContactsController@index')->name('api.metaperson.contacts.index');
            Route::post('/', 'Api\MetapersonContactsController@create')->name('api.metaperson.contacts.create');
            Route::get('/{id}', 'Api\MetapersonContactsController@show')->name('api.metaperson.contacts.show');
            Route::put('/{id}', 'Api\MetapersonContactsController@update')->name('api.metaperson.contacts.update');
            Route::delete('/{id}', 'Api\MetapersonContactsController@delete')->name('api.metaperson.contacts.delete');
        });

        Route::group(['prefix' => '{metaperson}/dates'], function () {
            Route::get('/', 'Api\MetapersonDatesController@index')->name('api.metaperson.dates.index');
            Route::post('/', 'Api\MetapersonDatesController@create')->name('api.metaperson.dates.create');
            Route::get('/{id}', 'Api\MetapersonDatesController@show')->name('api.metaperson.dates.show');
            Route::put('/{id}', 'Api\MetapersonDatesController@update')->name('api.metaperson.dates.update');
            Route::delete('/{id}', 'Api\MetapersonDatesController@delete')->name('api.metaperson.dates.delete');
        });
    });
});

