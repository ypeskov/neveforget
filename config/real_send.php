<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 22.06.16
 * Time: 16:39
 */

return [
    'is_enabled'        => true,   //when this value is false no any type of notification should be sent

    'email' => [
        'is_enabled'        => true,
        'only_white_list'   => env('MAIL_WHITE_LIST', true), //if enabled send only to white list recipients
    ],
    
    'sms'   => [
        'is_enabled'        => false,
        'only_white_list'   => env('SMS_WHITE_LIST', true), //if enabled send only to white list recipients
    ]
];