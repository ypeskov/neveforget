<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 22.06.16
 * Time: 16:54
 */

return [
    'email' => [
        'yuriy.peskov@gmail.com',
        'ypeskov@pisem.net',
        'rmalenko@gmail.com',
    ]
];
