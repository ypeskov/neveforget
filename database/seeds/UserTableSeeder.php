<?php

use \App\User;

class UserTableSeeder extends AbstractTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name'  => 'Yuriy',
            'middle_name' => '',
            'last_name'   => 'Peskov',
            'email'       => 'ypeskov@pisem.net',
            'timezone'    => 'Europe/Kiev',
            'password'    => bcrypt('123123'),
        ]);

        User::create([
            'first_name'  => 'Yuriy2',
            'middle_name' => '',
            'last_name'   => 'Peskov2',
            'email'       => 'yuriy.peskov@gmail.com',
            'timezone'    => 'Europe/London',
            'password'    => bcrypt('123123'),
        ]);

        User::create([
            'first_name'  => 'Michael',
            'middle_name' => '',
            'last_name'   => 'Sverdlikovskiy',
            'email'       => 'xedelweiss@gmail.com',
            'timezone'    => 'Africa/Algiers',
            'password'    => bcrypt('123123'),
        ]);
    }
}
