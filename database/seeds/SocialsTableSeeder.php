<?php

use App\User;

class SocialsTableSeeder extends AbstractTableSeeder
{
    const QTY_PER_USER = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

        foreach ($users as $user) {
            echo "\n---------------------------------------------\n";
            echo "Start creating socials for user: [{$user->email}]\n";
            $count = 0;
            factory(\App\Models\Social::class, self::QTY_PER_USER)
                ->create(['owner_id' => $user->id])
                ->each(function(\App\Models\Social $social) use (&$count) {
                    echo "Created: [{$social->social_type}:{$social->social_id}]\n";
                    $count++;
                });
            echo "Created $count socials\n";
            echo "---------------------------------------------\n";
        }
    }
}
