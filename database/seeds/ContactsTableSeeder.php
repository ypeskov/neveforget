<?php

use App\User;

class ContactsTableSeeder extends AbstractTableSeeder
{
    const QTY_PER_PERSON_SYNCED = 2;
    const QTY_PER_PERSON_SYNCED_OVERRIDDEN = 2;
    const QTY_PER_PERSON_NON_SYNCED = 4;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

        /**
         * @var $persons \App\Models\Person[]
         */
        $persons = new \Illuminate\Support\Collection();
        foreach ($users as $user) {
            $persons = $persons->merge($user->persons);
        }

        foreach ($persons as $person) {
            echo "\n---------------------------------------------\n";
            echo "Start creating contacts for person: [{$person->first_name} <{$person->nick_name}> {$person->last_name}]\n";
            $count = 0;

            // synced & not overridden
            factory(\App\Models\Contact::class, self::QTY_PER_PERSON_SYNCED)
                ->create(['person_id' => $person->id])
                ->each(function(\App\Models\Contact $contact) use (&$count, $person) {
                    $overriddenContact = factory(\App\Models\Contact::class)->create(['person_id' => $person->id]);
                    $social = \App\Models\Social::whereOwnerId($person->owner->id)->inRandomOrder()->first();

                    $contact->social_source_record_id = $social->id;
                    $contact->overridden_contact_id = $overriddenContact->id;
                    $contact->save();

                    echo "Created: [{$contact->contact_type}:{$contact->value}:synced:overridden]\n";
                    $count++;
                });

            // synced & overridden
            factory(\App\Models\Contact::class, self::QTY_PER_PERSON_SYNCED_OVERRIDDEN)
                ->create(['person_id' => $person->id])
                ->each(function(\App\Models\Contact $contact) use (&$count, $person) {
                    $social = \App\Models\Social::whereOwnerId($person->owner->id)->inRandomOrder()->first();

                    $contact->social_source_record_id = $social->id;
                    $contact->save();

                    echo "Created: [{$contact->contact_type}:{$contact->value}:synced:overridden]\n";
                    $count++;
                });

            // not overridden
            factory(\App\Models\Contact::class, self::QTY_PER_PERSON_NON_SYNCED)
                ->create(['person_id' => $person->id])
                ->each(function(\App\Models\Contact $contact) use (&$count, $person) {
                    echo "Created: [{$contact->contact_type}:{$contact->value}]\n";
                    $count++;
                });

            echo "Created $count contacts\n";
            echo "---------------------------------------------\n";
        }
    }
}
