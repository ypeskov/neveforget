<?php

class DateTypesTableSeeder extends AbstractTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // @todo think about built-in dates
        // built-in (is_custom==false) datetypes - are common date with it rules
        // title for such records is keyword

        // @todo think about recurring rules
        // https://github.com/simshaun/recurr
        // http://php.net/manual/ru/dateinterval.format.php

        \App\Models\DateType::create([
            'title' => 'birthday',
            'recurring_rules' => [],
            'is_custom' => false,
            'owner_id' => null,
        ]);

        \App\Models\DateType::create([
            'title' => 'weekly',
            'recurring_rules' => [],
            'is_custom' => false,
            'owner_id' => null,
        ]);

        \App\Models\DateType::create([
            'title' => 'monthly',
            'recurring_rules' => [],
            'is_custom' => false,
            'owner_id' => null,
        ]);

        \App\Models\DateType::create([
            'title' => 'yearly',
            'recurring_rules' => [],
            'is_custom' => false,
            'owner_id' => null,
        ]);

        Eloquent::reguard();
    }
}
