<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(SocialsTableSeeder::class);
        $this->call(PersonsTableSeeder::class);
        $this->call(DateTypesTableSeeder::class);
        $this->call(DatesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
    }
}
