<?php

use App\User;
use Illuminate\Database\Seeder;

/**
 * Class AbstractTableSeeder
 */
abstract class AbstractTableSeeder extends Seeder
{
    protected function getDemoUsers()
    {
        return [
            User::where('email', 'ypeskov@pisem.net')->firstOrFail(),
            User::where('email', 'yuriy.peskov@gmail.com')->firstOrFail(),
            User::where('email', 'xedelweiss@gmail.com')->firstOrFail(),
        ];
    }
}