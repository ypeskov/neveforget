<?php

use App\User;

class GroupsTableSeeder extends AbstractTableSeeder
{
    const QTY_PER_USER = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

        foreach ($users as $user) {
            echo "\n---------------------------------------------\n";
            echo "Start creating groups for user: [{$user->email}]\n";
            $count = 0;
            factory(\App\Models\Group::class, self::QTY_PER_USER)
                ->create(['owner_id' => $user->id])
                ->each(function(\App\Models\Group $group) use (&$count) {
                    echo "Created: [{$group->title}]\n";
                    $count++;
                });
            echo "Created $count groups\n";
            echo "---------------------------------------------\n";
        }
    }
}
