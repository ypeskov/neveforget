<?php

use App\User;

class DatesTableSeeder extends AbstractTableSeeder
{
    const QTY_PER_PERSON_BUILT_IN = 2;
    const QTY_PER_PERSON_CUSTOM = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

        /**
         * @var $persons \App\Models\Person[]
         */
        $persons = new \Illuminate\Support\Collection();
        foreach ($users as $user) {
            $persons = $persons->merge($user->persons);
        }

        foreach ($persons as $person) {
            echo "\n---------------------------------------------\n";
            echo "Start creating dates for person: [{$person->first_name} <{$person->nick_name}> {$person->last_name}]\n";
            $count = 0;

            // built-in
            factory(\App\Models\Date::class, self::QTY_PER_PERSON_BUILT_IN)
                ->create(['person_id' => $person->id])
                ->each(function(\App\Models\Date $date) use (&$count) {
                    echo "Created: [{$date->dateType->title}:{$date->date->format('d.m.Y H:i:s')}]\n";
                    $count++;
                });

            // custom
            factory(\App\Models\Date::class, self::QTY_PER_PERSON_CUSTOM)
                ->create(['person_id' => $person->id])
                ->each(function(\App\Models\Date $date) use (&$count, $person) {
                    $date->date_type_id = factory(\App\Models\DateType::class)->create(['owner_id' => $person->owner_id])->id;
                    $date->save();
                    echo "Created: [{$date->dateType->title}:{$date->date->format('d.m.Y H:i:s')}]\n";
                    $count++;
                });

            echo "Created $count dates\n";
            echo "---------------------------------------------\n";
        }
    }
}
