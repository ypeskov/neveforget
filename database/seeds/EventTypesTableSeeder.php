<?php

use App\Models\EventType;
use App\Models\Event;

class EventTypesTableSeeder extends AbstractTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::create([
            'id'                => Event::TYPE_EMAIL,
            'event_type_code'   => 'email',
            'event_type_name'   => 'email',
        ]);

        EventType::create([
            'id'                => Event::TYPE_SMS,
            'event_type_code'   => 'sms',
            'event_type_name'   => 'sms',
        ]);

        EventType::create([
            'id'                => Event::TYPE_IRC,
            'event_type_code'   => 'irc',
            'event_type_name'   => 'irc',
        ]);

        EventType::create([
            'id'                => Event::TYPE_FACEBOOK,
            'event_type_code'   => 'facebook',
            'event_type_name'   => 'facebook',
        ]);

        EventType::create([
            'id'                => Event::TYPE_TWITTER,
            'event_type_code'   => 'twitter',
            'event_type_name'   => 'twitter',
        ]);
    }
}
