<?php

use App\User;

class PersonsTableSeeder extends AbstractTableSeeder
{
    const QTY_PER_USER = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

        foreach ($users as $user) {
            echo "\n---------------------------------------------\n";
            echo "Start creating persons for user: [{$user->email}]\n";
            $count = 0;
            // not synced
            factory(\App\Models\Person::class, self::QTY_PER_USER)
                ->create(['owner_id' => $user->id])
                ->each(function(\App\Models\Person $person) use (&$count) {
                    echo "Created: [{$person->first_name} <{$person->nick_name}> {$person->last_name}]\n";
                    $count++;
                });

            // synced
            factory(\App\Models\Person::class, self::QTY_PER_USER)
                ->create(['owner_id' => $user->id])
                ->each(function(\App\Models\Person $person) use (&$count) {
                    $person->social_source_id = \App\Models\Social::whereOwnerId($person->owner_id)->inRandomOrder()->first()->id;
                    $person->last_synced_at = \Carbon\Carbon::now();
                    $person->save();

                    echo "Created: [{$person->first_name} <{$person->nick_name}> {$person->last_name}] (synced)\n";
                    $count++;
                });
            echo "Created $count empty persons\n";
            echo "---------------------------------------------\n";
        }
    }
}
