<?php

use App\User;
use App\Models\Event;

class EventsTableSeeder extends AbstractTableSeeder
{
    const QTY_EVENTS_PER_USER = 45;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getDemoUsers();

//        $emailEvent = EventType::where('event_type_code', 'email')->firstOrFail();

        foreach ($users as $user) {
            echo "\n---------------------------------------------\n";
            echo "Start creating events for user: [{$user->email}]\n";
            $count = 0;
            factory(Event::class, self::QTY_EVENTS_PER_USER)
                ->create(['user_id' => $user->id])
                ->each(function($event) use(&$count) {
                    echo "Created: [{$event->event_name}]\n";
                    $count++;
                });
            echo "Created $count events\n";
            echo "---------------------------------------------\n";
        }
    }
}
