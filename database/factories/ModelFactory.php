<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name'        => $faker->firstName,
        'last_name'         => $faker->lastName,
        'middle_name'       => $faker->domainName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'timezone'          => 'Europe/Kiev',
    ];
});

$factory->define(App\Models\Event::class, function(Faker\Generator $faker) {

    $eventDate = $faker->dateTimeBetween('-1year', '+2year')->format('Y-m-d H:00:00');

    return [
        'timezone'          => 'Europe/Kiev',
        'event_name'        => $faker->sentence(2),
        'recipient'         => $faker->email,
        'message_subject'   => $faker->sentence(4),
        'message_body'      => $faker->sentence(6),
        'message_signature' => $faker->name,
        'datetime_to_send'  => $eventDate,
        'event_types_id'    => 1,
        'is_active'         => 1,
        'comment'           => $faker->paragraph(3),
        // 'user_id'
    ];
});

$factory->define(App\Models\Person::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'nick_name' => $faker->userName,
        'job_title' => $faker->jobTitle,
        'company' => $faker->company,
        'notes' => $faker->sentence(6),
        // 'owner_id'
        // 'social_source_id'
        // 'social_source_record_id'
        // 'last_synced_at'
    ];
});

$factory->define(App\Models\Social::class, function (Faker\Generator $faker) {
    return [
        'social_type' => $faker->randomElement(\App\Models\Social::types()),
        'social_id' => $faker->randomElement([$faker->randomNumber(6), $faker->userName]),
        'info' => [],
        'auth_info' => [],
        // 'owner_id'
    ];
});

$factory->define(App\Models\DateType::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(mt_rand(1, 2), true),
        'recurring_rules' => [],
        'is_custom' => true,
        // 'owner_id',
    ];
});

$factory->define(App\Models\Date::class, function (Faker\Generator $faker) {
    $custom = \App\Models\DateType::whereIsCustom(false)->inRandomOrder()->first();

    return [
        'date_type_id' => $custom ? $custom->id : factory(App\Models\DateType::class)->create()->id,
        'date' => $faker->dateTimeThisDecade,
        // 'person_id'
        // 'overridden_contact_id'
        // 'person_id'
        // 'social_source_record_id'
    ];
});

$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {
    $contactType = $faker->randomElement(\App\Models\Contact::types());

    switch ($contactType) {
        case \App\Models\Contact::TYPE_EMAIL:
            $value = $faker->email;
            break;
        case \App\Models\Contact::TYPE_SKYPE:
            $value = $faker->userName;
            break;
        default:
            $value = $faker->randomNumber(6);
    }

    return [
        'contact_type' => $contactType,
        'value' => $value,
        // 'person_id'
        // 'overridden_contact_id'
        // 'person_id'
        // 'social_source_record_id'
    ];
});

$factory->define(App\Models\Group::class, function (Faker\Generator $faker) {
    $title = $faker->text(20);
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'owner_id' => factory(\App\User::class)->create()->id,
    ];
});