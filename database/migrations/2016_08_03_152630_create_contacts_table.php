<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('metacontact_id')->unsigned();
            $table->enum('contact_type', ['phone', 'email', 'vkontakte', 'facebook', 'skype', 'viber', 'telegram', 'whatsapp']); // not the same as social type
            $table->string('value');
            $table->string('overridden_value')->nullable(); // user can "fix" synced value and it should not be show with original value
            $table->integer('sync_source_id')->unsigned()->nullable();
            $table->timestamp('last_synced_at')->nullable();

            $table->foreign('metacontact_id')
                ->references('id')
                ->on('metacontacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('sync_source_id')
                ->references('id')
                ->on('socials')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
