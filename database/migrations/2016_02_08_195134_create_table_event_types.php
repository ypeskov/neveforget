<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\EventType;

class CreateTableEventTypes extends Migration
{
    const TABLE_NAME = EventType::TABLE_NAME;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');

            $table->string('event_type_name', EventType::EVENT_TYPE_NAME_MAX_LENGTH);
            $table->string('event_type_code', EventType::EVENT_TYPE_CODE_MAX_LENGTH);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
