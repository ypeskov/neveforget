<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Event;
use App\Models\EventType;

class AddEventTypeToEvents extends Migration
{
    const TABLE_NAME = Event::TABLE_NAME;
    const EVENT_TYPE_ID_COLUMN  = '_id';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table
                ->integer(EventType::TABLE_NAME . self::EVENT_TYPE_ID_COLUMN)
                ->after('id')
                ->unsigned();

            $table
                ->foreign(EventType::TABLE_NAME . self::EVENT_TYPE_ID_COLUMN)
                ->references('id')
                ->on(EventType::TABLE_NAME)
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table
                ->boolean('is_active')
                ->after('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function(Blueprint $table) {
            $table
                ->dropForeign(self::TABLE_NAME . '_'
                    . EventType::TABLE_NAME
                    . self::EVENT_TYPE_ID_COLUMN
                    . '_foreign'
                );

            $table->dropColumn(EventType::TABLE_NAME . self::EVENT_TYPE_ID_COLUMN);

//            $table->dropColumn('is_active');
        });
    }
}
