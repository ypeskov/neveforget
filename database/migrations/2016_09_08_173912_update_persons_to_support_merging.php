<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonsToSupportMerging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->integer('parent_person_id')->unsigned()->nullable();
            $table->boolean('is_hidden')->default(false); // user can "delete" synced person and it should not be shown

            $table->foreign('parent_person_id')
                ->references('id')
                ->on('persons')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropForeign(['parent_person_id']);
            $table->dropColumn('parent_person_id');
            $table->dropColumn('is_hidden');
        });
    }
}
