<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMetacontactsTableMerging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metacontacts', function (Blueprint $table) {
            $table->integer('social_source_id')->unsigned()->nullable(); // social where metacontact was imported from
            $table->string('social_source_record_id')->nullable(); // metacontact identifier in social api
            $table->timestamp('last_synced_at')->nullable();

            $table->foreign('social_source_id')
                ->references('id')
                ->on('socials')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metacontacts', function (Blueprint $table) {
            $table->dropForeign(['social_source_id']);

            $table->dropColumn('social_source_id');
            $table->dropColumn('social_source_record_id');
            $table->dropColumn('last_synced_at');
        });
    }
}
