<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    protected function getTableName()
    {
        return (new User())->getTable();
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->getTableName(), function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name', User::FIRST_NAME_LENGTH);
            $table->string('last_name', User::LAST_NAME_LENGTH);
            $table->string('middle_name', User::MIDDLE_NAME_LENGTH);
            $table->string('email')->unique();
            $table->string('password', User::PASSWORD_LENGTH);
            $table->rememberToken();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->getTableName());
    }
}
