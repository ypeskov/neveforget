<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactsToSupportOverriding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->integer('overridden_contact_id')->nullable()->unsigned();

            $table->foreign('overridden_contact_id')
                ->references('id')
                ->on('contacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->dropColumn('overridden_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('overridden_value')->nullable();

            $table->dropForeign(['overridden_contact_id']);
            $table->dropColumn('overridden_contact_id');
        });
    }
}
