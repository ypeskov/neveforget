<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactsTableMerging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign(['sync_source_id']);

            $table->dropColumn('sync_source_id');
            $table->dropColumn('last_synced_at');

            $table->boolean('is_hidden')->default(false); // user can "delete" synced contact and it should not be shown
            $table->string('social_source_record_id')->nullable(); // contact identifier in social api (if needed)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->integer('sync_source_id')->unsigned()->nullable();
            $table->timestamp('last_synced_at')->nullable();

            $table->dropColumn('is_hidden');
            $table->dropColumn('social_source_record_id');

            $table->foreign('sync_source_id')
                ->references('id')
                ->on('socials')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
}
