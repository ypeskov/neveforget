<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Event;

class UpdateEventsTableWithTimezone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Event::TABLE_NAME, function(Blueprint $table) {
            $table->string('timezone', 64)->after('datetime_to_send')->index(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Event::TABLE_NAME, function(Blueprint $table) {
            $table->dropColumn('timezone');
        });
    }
}
