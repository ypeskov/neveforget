<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('metacontact_id')->unsigned();
            $table->integer('date_type_id')->unsigned();
            $table->dateTime('date');

            $table->foreign('metacontact_id')
                ->references('id')
                ->on('metacontacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('date_type_id')
                ->references('id')
                ->on('date_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dates');
    }
}
