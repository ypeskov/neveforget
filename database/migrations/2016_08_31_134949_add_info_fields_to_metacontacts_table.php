<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoFieldsToMetacontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metacontacts', function (Blueprint $table) {
            $table->string('job_title');
            $table->string('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metacontacts', function (Blueprint $table) {
            $table->removeColumn('job_title');
            $table->removeColumn('company');
        });
    }
}
