<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMetacontactsToPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // drop keys

        Schema::table('dates', function (Blueprint $table) {
            $table->dropForeign(['metacontact_id']);
            $table->renameColumn('metacontact_id', 'person_id');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('contact_type'); // Laravel/Doctrine can't rename anything if table contains enum
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign(['metacontact_id']);
            $table->renameColumn('metacontact_id', 'person_id');
        });

        // rename table

        Schema::table('metacontacts', function (Blueprint $table) {
            $table->rename('persons');
        });

        // restore keys

        Schema::table('dates', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')
                ->on('persons')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->enum('contact_type', ['phone', 'email', 'vkontakte', 'facebook', 'skype', 'viber', 'telegram', 'whatsapp']); // not the same as social type
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('person_id')
                ->references('id')
                ->on('persons')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop keys

        Schema::table('dates', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
            $table->renameColumn('person_id', 'metacontact_id');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('contact_type'); // Laravel/Doctrine can't rename anything if table contains enum
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
            $table->renameColumn('person_id', 'metacontact_id');
        });

        // rename table

        Schema::table('persons', function (Blueprint $table) {
            $table->rename('metacontacts');
        });

        // restore keys

        Schema::table('dates', function (Blueprint $table) {
            $table->foreign('metacontact_id')
                ->references('id')
                ->on('metacontacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->enum('contact_type', ['phone', 'email', 'vkontakte', 'facebook', 'skype', 'viber', 'telegram', 'whatsapp']); // not the same as social type
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('metacontact_id')
                ->references('id')
                ->on('metacontacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
}
