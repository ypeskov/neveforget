<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Event;

class CreateEventsTable extends Migration
{
    const TABLE_NAME = Event::TABLE_NAME;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('event_name', Event::EVENT_NAME_MAX_LENGTH);
            $table->string('recipient', Event::RECIPIENT_MAX_LENGTH);
            $table->string('message_subject', Event::MESSAGE_SUBJECT_MAX_LENGTH);
            $table->text('message_body');
            $table->string('message_signature', Event::MESSAGE_SIGNATURE_MAX_LENGTH);
            $table->dateTime('datetime_to_send');
            $table->string('comment', Event::COMMENT_LENGTH);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
