### React UI
https://bitbucket.org/xedelweiss/neverforget-client

### Debug
Clockwork extension:
https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp

### Preparation
    php artisan jwt:secret

### Development tools
    php artisan ide-helper:generate     // generate phpStorm helper file
    php artisan ide-helper:models -W -R // generate phpDocs for models
    php artisan ide-helper:meta         // generate meta for container autocomplete
    
### Todo

- Overriding for dates