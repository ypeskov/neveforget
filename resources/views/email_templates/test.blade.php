<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

</head>
<body>
    <div>{!! $messageContent !!}</div>

    <br />
    <br />

    <div>{{ $signature }}</div>
</body>
</html>