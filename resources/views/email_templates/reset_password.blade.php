<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

</head>
<body>
  <div>
    <div>{{ trans('auth.reset_email_content_name', ['first_name' => $first_name, 'last_name' => $last_name, ]) }}</div>

    <br />

    <div>{{ trans('auth.reset_email_content_description') }}</div>

    <br />
    <div><a href="{{$url}}/password/change?token={{$token}}">{{ trans('auth.reset_password') }}</a></div>
    <br />

    <div>{{trans('auth.reset_email_content_footer_line1')}}</div>
    <div>{{trans('auth.reset_email_content_footer_line2')}}</div>
  </div>
</body>
</html>