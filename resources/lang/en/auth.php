<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    //Password reset email
    'reset_password_link_email_subject' => 'Someone has requested the change of your password',
    'reset_email_content_name'  => 'Dear :first_name :last_name,',
    'reset_email_content_description'
        => 'Someone has request the change of your password on our service.'
            . ' If you unaware of this please ignore this email.'
            . ' If it was You please click the link below',
    'reset_password'    => 'Reset password',
    'reset_email_content_footer_line1'    => 'Sincerely yours,',
    'reset_email_content_footer_line2'    => 'Neverforget.date',
];
